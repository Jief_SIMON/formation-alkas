// Une chaîne de caractères est similaire à un tableau de caractères.
// On définit généralement une chaîne comme du texte entre guillemets ou apostrophes.
let exemple = "Coucou"
let exemple2 = 'aujourd\'hui' // Si on utilise ' pour délimiter notre string, on doit échapper les caractères ' donc notre string pour ne pas la fermer par inadvertance.

// Pour concaténer des chaînes ensemble, on utilise +
// On peut également concaténer des nombres avec des chaînes
let concatenation = "ceci est " + 1 + " chaîne" 
// équivaut à "ceci est 1 chaîne"

// On peut également contaténer des variables contenant des chaînes ou autres.
let politesse = "Comment ça va " + exemple2 + " ?"
// équivaut à Comment ça va aujourd'hui ?

// À la place d'une concaténation on peut également utiliser les `` pour insérer des variables dans une chaîne.
// ${variable} dans une chaîne définie avec des back quotes permet d'insérer ladite variable.
// ${} permet également d'exécuter du code, des conditions ou des fonctions.
let politesse2 = `Comment ça va ${exemple} ?`

// Fonctions liées au string :

// Manipuler la casse (minuscule/majuscule).
// toUpperCase() permet de passer une chaîne en majuscule.
// toLowerCase() permet de passer une chaîne en minuscule.
let petitBonjour = "bonjour"
petitBonjour.toUpperCase() // renvoie "BONJOUR"

let grosBonjour = "COUCOU"
grosBonjour.toLowerCase() // renvoie "coucou"

// La comparaison de chaînes prend en compte la case
"toto" === "Toto" // false
"toto" === "Toto".toLowerCase() // renvoie true

// Etant donné que les chaînes de caractères sont représentées comme des tableaux de caractères, on peut accéder à un caractère en particulier à l'aide d'un indice numérique.

let phrase = "Balkany est en liberté"
phrase[0] // renvoie "B"
phrase[6] // renvoie "y"
phrase[11] // renvoie " "
phrase[phrase.length - 1] // renvoie le dernier caractère "é"

// On peut également parcourir une chaîne à l'aide d'un for,
for (let i = 0; i < phrase.length; i++) {
    console.log(phrase[i]) // affiche chaque caractère
}

// On peut séparer une chaîne en sous parties à l'aide de split()
// split() permet de séparer une chaîne en sous parties délimitées par un caractère précis.
// Par exemple, pour récupérer tous les mots de notre phrase, pn split la chaîne au niveau des espaces.
let mots = phrase.split(" ");
// mots contient ["Balkany", "est", "en", "liberté"]
let numero = "06.06.06.06.06"
numero.split(".") // renvoie ["06", "06", "06", "06", "06"]