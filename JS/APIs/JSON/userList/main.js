// Récupération des données stockées dans notre fichier MOCK_DATA.json
const users = data;

// On attend que le Document soit entièrement chargé,
document.addEventListener("DOMContentLoaded", () => {
    
    const userSearch = document.getElementById("userSearch");
    
    const userWoman = document.getElementById("userWoman");
    const userMan = document.getElementById("userMan");
    
    userWoman.addEventListener("change", (event) => {
        console.log(event.target);
        if (event.target.checked === true) {
            const womanResults = [];
            
            users.forEach((user) => {
                if (user.gender.includes("Female")) {
                    womanResults.push(user);
                }
            });
            displayUsers(womanResults);
        } else {
            displayUsers(users);
        }
    });

    userMan.addEventListener("change", (event) => {
        console.log(event.target);
        if (event.target.checked === true) {
            const manResults = [];
            
            users.forEach((user) => {
                if (user.gender.includes("Male")) {
                    manResults.push(user);
                }
            });
            displayUsers(manResults);
        } else {
            displayUsers(users);
        }
    });

    userSearch.addEventListener("keyup", () => {
        const searchTerm = userSearch.value.toLowerCase();

        let searchResults = [];
        
        users.forEach((user) => {
            const fullName = `${user.first_name} ${user.last_name}`.toLowerCase();
            if (fullName.includes(searchTerm)) {
                searchResults.push(user);
            }
        });

        displayUsers(searchResults);
    });

    displayUsers(users);
});

function displayUsers(userList) {
    const userContainer = document.getElementById("userContainer");

    userContainer.innerHTML = "";
    // Pour chaque user de noter tableau de users,
    userList.forEach((user) => {
        // Création d'un élément div
        const userCard = document.createElement("div");
        userCard.id = "userCard";
        // Création d'un élément h2
        const userTitle = document.createElement("h2");
        userTitle.id = "userTitle";
        userTitle.textContent = `${user.first_name} ${user.last_name}`;
        
        const userImage = document.createElement("img");
        userImage.id = "userImage";
        userImage.src = user.avatar;
        userImage.alt = `${user.first_name}'s avatar`;
        
        const userGender = document.createElement("img");
        userGender.className = "userGender";
        if (user.gender === "Female") {
            userGender.id = "female";
            userGender.src = "../userList/userImages/venus.svg";
        } else {
            userGender.id = "male";
            userGender.src = "../userList/userImages/mars.svg";
        };
        userGender.alt = `${user.first_name}'s gender is ${user.gender}`;

        userCard.appendChild(userImage);
        userCard.appendChild(userTitle);
        userCard.appendChild(userGender);

        userContainer.appendChild(userCard);
    });
}