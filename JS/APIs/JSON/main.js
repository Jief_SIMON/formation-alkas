// JavaScript Objet Notation (JSON) est un format standard utilisé pour représenter des données structurées de façon semblables aux objets JavaScript.
// Il est souvent utilisé pour structurer et transmettre des données dans le cadre d'applications web.
// Il es tainsi possible de tranférer facilement des données entre applications utilisant des technologies différentes, en passant par HTTP et en envoyant du JSON (qui est du texte).

// Le JSON ressemblant à un objet JavaScript, on va en générer à partir d'un objet :
let user = {
    name: "Jack",
    dob: "1965-06-25",
    adress: {
        city: "Montpellier",
        street: "Rue de j'men fous",
        country: "FR",
    },
    books: [
        {
            title: "Les androïdes rêvent-il de moutons electriques ?",
            author: "Philip K. Dick",
        },
        {
            title: "Ubik",
            author: "Philip K. Dick",
        },
    ],
};

// L'interprétation de cet objet en texte formaté en JSON peut s'appeler sérialisation.
// Pour sérialiser un objet en JSON on peut utiliser JSON.stringify en javascript.
let userJson = JSON.stringify(user); //renvoie une chaîne de caractères représentant mon objet.

// Pour désérialiser (texte cvers objet) un texte formaté en JSON, on peut utiliser JSON.parse en javascript.
let newUser = JSON.parse(user); //renvoie l'objet représenté par ma chaîne de caractères.