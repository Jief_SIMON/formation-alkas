// AJAX (Asynchronous Javascript And XML)
// C'est un principe de programmation quie préconnise l'utilisation de requête asynchron de façon à pouvoir faire transiter et manipuler des données dans uyne application; sans paralyser le fil d'exécution ou obliger un rafraîchissement de la page à chaque nouvelle action ou donnée.
// Il existe de nombreuses technologies permettant d'appliquer ce genre de principe.
// L'une d'entre elles est la suivante :
// Pour envoyer une requête HTTP Asynchrone, on peut utiliser XMLHttpRequest (XHR).
// Pour créer une XHR, on peut :
const request = new XMLHttpRequest();

// Pour définir la méthode et l'adresse de destination, on ouvre la requête en utilisant OPEN.
request.open("GET", "https://randomuser.me/api");

// Pour envoyer la requête, on utilise ensuite SEND.
request.send();

// Étant donné que la requête est envoyée sur le réseau, il est impossible de récupérer les informations de façon instantanée.
// Si on tente de récupérer la réponse directement, elle sera vide car pas encore arrivée au moment de l'exécution de notre code.
console.log("récupération synchrone", request.response);

// Pour réagir à la réponse de façon asynchrone, on peut donc utiliser un eventListener avec un callBack.
// En écoutant l'event load, on indique vouloir exécuter un callBack lorsque la réponse est arrivée.
// On peut également exécuter l'event error pour réagir à une erreur, progress pour écouter le chargement d'une requête, abort pour réagir à une annulation.
// https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest/Utiliser_XMLHttpRequest pour plus d'informations
request.addEventListener("load", () => {
    // On peut ensuite accéder à la réponse de cette façon par exemple :
    console.log("récupération asynchrone", JSON.parse(request.response));
    // Une fois le json parsé ici, on peut commencer à manipuler nos objets.
});

//une façon plus moderne d'envoyer des requêtes HTTP existent via l'api Fetch https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch
// Une façon plus moderne d'envoyer des requêtes HTTP existent via l'api Fetch
// Une requête fetch basique peut s'initier de la façon suivante :
fetch("https://randomuser.me/api").then((response) => {
    return response.json();
}).then((data) => {
    console.log(data);
});