const apikey = "8418975cebf45353f4edf840632ee85f";
const lat = "43.61092";
const lon = "3.87723";

document.addEventListener("DOMContentLoaded", () => {

    const searchInput = document.getElementById("search");
    const searchGo = document.getElementById("go");
    const searchResults = document.getElementById("serach-results");
    
    searchInput.addEventListener("click", () => {
        console.log(searchInput);

        fetch(`https://nominatim.openstreetmap.org/search?q=montpellier&format=jsonv2&accept-language=fr-FR&limit=1`
        ).then(response => {
            response.json().then((weatherLocation) => {
                console.log(weatherLocation);
                const lon = weatherLocation.lon;
                const lat = weatherLocation.lat;
            });
        });
    })

    const currentTemp = document.getElementById('current-temp');
    const currentWeatherCondition = document.getElementById('current-weather-condition');
    const currentWindDir = document.getElementById('current-wind-dir');
    const currentWindSpeed = document.getElementById('current-wind-speed');
    const currentIcon = document.getElementById('current-icon');

    fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${apikey}&exclude=minutely&units=metric&lang=fr`
    ).then(response => {
        response.json().then(
            weatherData => {
                console.log(weatherData);

                currentTemp.textContent = formatTemp(weatherData.current.temp);
                currentIcon.src = `http://openweathermap.org/img/wn/${weatherData.current.weather[0].icon}@2x.png`;
                currentWeatherCondition.textContent = weatherData.current.weather[0].description;
                currentWindDir.textContent = weatherData.current.wind_deg;
                currentWindSpeed.textContent = weatherData.current.wind_speed;
                
                displayDaily(weatherData);

                displayHourly(weatherData);
            }
        )
    });
})

function displayDaily(weatherData) {
    const dailyContainer = document.getElementById('dailyContainer');
    dailyContainer.innerHTML = "";

    weatherData.daily.forEach(dailys => {
        const daily = document.createElement("div");
        daily.classList.add("daily");

        const dailyTemp = document.createElement("div");
        dailyTemp.classList.add("daily-temp");
        dailyTemp.textContent = formatTemp(dailys.temp.day);

        const dailyWeatherCondition = document.createElement("div");
        dailyWeatherCondition.classList.add("daily-weather-condition");
        dailyWeatherCondition.textContent = dailys.weather[0].description;

        const dailyIcon = document.createElement("img");
        dailyIcon.classList.add("daily-icon");
        dailyIcon.src = `http://openweathermap.org/img/wn/${dailys.weather[0].icon}@2x.png`;

        const dailyWind = document.createElement("div");
        dailyWind.classList.add("daily-wind");
        dailyWind.textContent = dailys.wind_deg;

        daily.appendChild(dailyTemp);
        daily.appendChild(dailyIcon);
        daily.appendChild(dailyWeatherCondition);
        daily.appendChild(dailyWind);

        dailyContainer.appendChild(daily)
                    
    });
}

function displayHourly(weatherData) {
    const hourlyContainer = document.getElementById("hourlyContainer");
    hourlyContainer.innerHTML = "";

    weatherData.hourly.forEach(hourlys => {
        const hourly = document.createElement("div");
        hourly.classList.add("hourly");

        const hourlyTemp = document.createElement("div");
        hourlyTemp.classList.add("hourly-temp");
        hourlyTemp.textContent = formatTemp(hourlys.temp);

        const hourlyWeatherCondition = document.createElement("div");
        hourlyWeatherCondition.classList.add("hourly-weather-condition");
        hourlyWeatherCondition.textContent = hourlys.weather[0].description;
        
        const hourlyIcon = document.createElement("img");
        hourlyIcon.classList.add("hourly-icon");
        hourlyIcon.src = `http://openweathermap.org/img/wn/${hourlys.weather[0].icon}@2x.png`;

        const hourlyWind = document.createElement("div");
        hourlyWind.classList.add("hourly-wind");
        hourlyWind.textContent = hourlys.wind_deg;

        hourly.appendChild(hourlyTemp);
        hourly.appendChild(hourlyIcon);
        hourly.appendChild(hourlyWeatherCondition);
        hourly.appendChild(hourlyWind);

        hourlyContainer.appendChild(hourly);
    })
}

function formatTemp(temp) {
    return `${Math.floor(temp)}°C`;
}