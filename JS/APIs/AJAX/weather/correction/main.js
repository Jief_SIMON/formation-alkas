const apikey = "8418975cebf45353f4edf840632ee85f";

document.addEventListener("DOMContentLoaded", () => {
    const searchInput = document.getElementById("search");
    const searchGo = document.getElementById("go");
    const searchResults = document.getElementById("search-results");

    searchGo.addEventListener("click", () => {
        getGeoData(searchInput.value).then((geoData) => {
            searchResults.innerHTML = "";
            geoData.forEach((place) => {
                const placeLi = document.createElement("li");
                placeLi.textContent = place.display_name;
                searchResults.appendChild(placeLi);
                placeLi.addEventListener("click", () => {
                    getForecast(place).then((weatherData) => {
            
                        displayForecast(weatherData.daily, place);
                        
                        displaySelected(weatherData.daily[0]);

                        searchResults.innerText = "";

                        const dailyContainer = document.getElementById("dailies");
                        
                        [...dailyContainer.children].forEach(
                            (dailyDiv, index) => {
                                dailyDiv.addEventListener('click', (event) => {
                                    dailyContainer
                                        .querySelector(".selected")
                                        .classList.remove("selected");
                                    event.currentTarget.classList.add(
                                        "selected"
                                    );
                                    
                                    displaySelected(weatherData.daily[index]);
                                });
                            });
                    });
                });
            });
        });
    });
});

function getGeoData(searchTerm) {
    return fetch(
        `https://nominatim.openstreetmap.org/search?format=jsonv2&q=${searchTerm}`
    ).then((response) => {
        return response.json(); 
    });
}

function getForecast(place) {
    return fetch(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${place.lat}&lon=${place.lon}&appid=${apikey}&exclude=minutely&units=metric&lang=fr`
    ).then((response) => {
        return response.json();
    });
}
    
function displayForecast(dailies, place) {
    
    document
        .getElementById("daily-view")
        .querySelector(".location").textContent = place.display_name;

    const dailyContainer = document.getElementById("dailies");
    const dailyDivs = dailyContainer.children;
    
    dailies.forEach((daily, index) => {
        dailyDivs[index].querySelector(".daily-max").textContent = formatTemp(daily.temp.max);
        
        dailyDivs[index].querySelector(".daily-min").textContent = formatTemp(daily.temp.min);
        
        dailyDivs[index].querySelector(".daily-icon > img").src = `http://openweathermap.org/img/wn/${daily.weather[0].icon}@2x.png`;
        
        dailyDivs[index].querySelector(".day-label").textContent = getShortDay(daily.dt);
    });
}

function displaySelected(daily) {
    const dailyView = document.getElementById("daily-view");

    dailyView.querySelector(".report-temp").textContent = formatTemp(daily.temp.day);

    dailyView.querySelector(".report-icon > img").src = `http://openweathermap.org/img/wn/${daily.weather[0].icon}@2x.png`;
    
    dailyView.querySelector(".humidity").textContent = `Humidité : ${daily.humidity}%`;
    
    dailyView.querySelector(".rain").textContent = `Précipitations : ${
        daily.rain ? daily.rain : 0
    } mm`;
    
    dailyView.querySelector(".wind").textContent = `Vent : ${
        daily.wind_speed ? Math.round(daily.wind_speed) : 0
    } km/h`;
    
    dailyView.querySelector(".condition").textContent = daily.weather[0].description;
    
    dailyView.querySelector(".day").textContent = getShortDay(daily.dt);
}

function formatTemp(temp) {
    return `${Math.floor(temp)}°C`;
}

function getShortDay(timestamp) {
    const dailyDate = new Date(timestamp * 1000);
        
    const dailyDay= dailyDate.toLocaleDateString("fr-FR", {
        weekday: "short",
    });
    return dailyDay;
}