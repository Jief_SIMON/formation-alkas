// Ceci est un commentaire
/*
Ceci est un cpmmentaire multiligne
Un commentaire multiligne n'est pas interprété lors de l'exécution du code
et sert donc souvent à documenter le code
*/

// VARIABLES

// En JavaScript, une variable se déclare à l'aide du mot clef "let"
let nombre = 10 // Ici, on définit une variable nommée nombre contenant comme valeur 10
// En JavaScript, les données sont typées dynamiquement, 
// c'est-à-dire que JavaScript définit le type de la donnée automatiquement selon la donnée stockées.
// Dans un langage typé statiquement, comme le C, on devrait déterminer le type de vairable au moment de la déclaration :
// Ex : int nombre = 10;

let message = "coucou" // ici, on définit une variable nommée message contenant une chaîne de caractère "coucou"
// Une chaîne de caractère peut se définir à l'aide de simple quote (')
// de double quote (")
// ou back quote (`) ==> altgr + 7

// Pour utiliser une valeur stockée dans une variable, on appelle tout simplement son nom
let a = 5
let b = 3
let c = a + b // c contient la valeur 8

// Pour définir une constante (une variable ne pouvant pas changer) on utilise le mot clef const
const neChangeraPas = 12
neChangeraPas = 13 // Impossible : JavaScript empêche de changer une constante


// CONDITIONS

// Un branchement conditionnel en javasript se fait à l'aide d'un if (la plupart du temps)
// Une condition se fait sur une comparaison ou sir n'importe quelle opération renvoyant un booléen (true ou false)
// Pour effectuer des comparaisons, js possède différents opérateurs
/*
	== égalité
	!= différence
	> supériorité stricte
	< infériorité stricte
	>= supériorité ou égalité
	<= supériorité ou égalité
	=== égalité stricte
	!== différence stricte
*/
let age = 12
// pour écrire une condition avec un Si
// on écrit if (condition) {  }
// et on écrit les instructions entre les accolades
if (age < 18) {
	// ici on indique que faire si la condition est validée
	// revenez plus tard
} else {
	// ici on indique quoi faire dans le cas contraire
	// go
	if (age > 21) {
		// on peut imbriquer des conditions dans d'autres conditions
		//vous avez le droit de boire aux usa
	}
}


// BOOLEENS

// Pour pouvoir gérer des conditions complèxes, on peut associer des conditions entre elles avec des opérateurs
// de logique tirés de l'algèbre de Boole

// L'opérateur "et" s'inscrire &&
// true && true === true
// true && false === false
// false && true === false
// false && false === false

// L'opérateur "ou" s'inscrit ||
// true || true === true
// true || false === true
// false || false === false

// Le "non" s'exprime avec !
// !true === false
// !false === true
// !(true && false) === true
// !(true || false) === false


// BOUCLES

// Pour écrire une boucle, on peut utiliser While ou For
let i = 1
while (i <= 100) {
	// on affiche i par exemple
	i = i + 1 // on ajoute 1 à i (incrémentation)
}

// La boucle for quant à elle permet d'effectuer des opérations répétées comme le while
// mais sa syntaxe permet de définir un départ, un arrêt, et une opération à effectuer à chaque fois, le tout
// sur une seule ligne
// for (départ; arrêt; changement) {}
for (let i = 1; i <= 100; i++) {
	// instructions de la boucle
}


// FONCTIONS

// Une fonction se déclare à l'aide du mot clef function
// Les paramètres d'entrée d'une fonction se définissent entre paranthèses à la suite du nom de la fonction
function additionner(a, b){
	// le mot clef return met fin à l'exécution de la fonction
	// et renvoie la valeur précisée
	return a + b
}

// en invoquant une fonction avec une valeur de retour (return), cette fonction renvoie la valeur de retour
// comme le ferait une variable
additionner(1, 2) // renverra 3
additionner(3, -6) // renverra -3
additionner(additionner(1, 2), additionner(3, 3)) // renverra 9