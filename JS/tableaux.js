// Un tableaux est une structure de données de taille définie
// permettant de stocker un jeu de données de même type.
// En JavaScript, (et en programmation en général) un tableau 
// se déclare à l'aide de crochet [].
let tableauVide = [] // Déclaration d'un tableau vide

// Un tableau contient des "cases", chaque case contient une donnée
// chaque case est séparée par une virgule.
//                   0         1        2
let listeCourses = ['éponge', 'pâtes', 'sauce tomate']
// Pour accéder aux données stockées dans le tableau, on utilise un indice.
listeCourses [0] // Contient "éponges"
listeCourses [1] // Contient "pâtes"
listeCourses [2] // Contient "sauce tomate"
listeCourses [3] // Renvoie undefined (non défini)

// Pour récupérer la taille d'un tableau, on peut utiliser array.length
listeCourses.length // renvoie 3
// Le dernier élément d'un tableau se trouve à array[array.length - 1]

// Pour parcourir un tableau, une bonne méthode est d'utiliser une boucle.
// La boucle for semble toute indiquée,
// et on avance case par case.

for (let i = 0; i < listeCourses.length; i++) {
    // Pour chaque tour de boucle, on affichera une case du tableau.
    console.log(listeCourses[i])
}

// Pour rajouter un élément à la fin d'un tableau,
// on utilise array.push(element)
listeCourses.push("sel") // listeCourses contient ['éponge', 'pâtes', 'sauce tomate', 'sel']
listeCourses[3] // Renvoie "sel"

// Pour retirer le dernier élément d'un tableau,
// on utilise array.pop()
listeCourses.pop() // renvoie et supprime le dernier élément du tableau
// listeCourses contient ['éponge', 'pâtes', 'sauce tomate']

// Ajouter un élément au début du tableau demande l'utilisation de
// array.unshift(élément)
listeCourses.unshift('whisky')
// listeCourses contient ['whisky', 'éponge', 'pâtes', 'sauce tomate']

// Supprimer le premier élément du tableau se fait à l'aide de 
// array.shift()
listeCourses.shift() // renvoie et supprime le premier élément de la liste.
// listeCourses contient ['éponge', 'pâtes', 'sauce tomate']

// array.splice() permet de raccorder, supprimer et remplacer des éléments dans le tableau.
// Pour remplacer un élément par un autre, par exemple :
// À la case 1, on supprime l'élément, et insère l'élément "pâtes complètes" 
listeCourses.splice(1, 1, 'pâtes complètes')
// listeCourses contient ['éponge', 'pâtes complètes', 'sauce tomate']
// On peut séparer les éléments à insérer par des virgules pouyr en insérer plusieurs.
// À la case 1, on supprime 0 éléments, et on insère "parmesan" et "pesto"
listeCourses.splice(1, 0, 'parmesan', 'pesto')
// listeCourses contient ['éponge', 'parmesan', 'pesto', 'pâtes complètes', 'sauce tomate']
// splice renvoie également tous les éléments supprimés.

// array.slice() renvoie une partie d'un tableau
// en faisant une copie des éléments à partir d'une borne inférieure (incluse) jusqu'à une borne supérieure (exclue)
listeCourses.slice(1, 4) // renvoie ['parmesan', 'pesto', 'pâtes complètes']
