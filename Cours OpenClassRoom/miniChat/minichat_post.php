<?php

if (isset($_POST['user']) && isset($_POST['message'])) {
    $user = $_POST['user'];
    $message = $_POST['message'];

    if (!empty($user) && !empty($message)) {
        include('minichatConnect.php');

        $dbh = dbConnect();

        $newMessage = "INSERT INTO messages (user, message, sent_at) VALUES (:user, :message, :sent_at)";

        $stmt = $dbh->prepare($newMessage);

        $sent_at = date("Y-m-d H:i:s");
        $stmt->execute(
            [
                ":user" => $user,
                ":message" => $message,
                ":sent_at" => $sent_at,
            ]
        );

        header('Location: minichat.php');
        exit;
    }
}