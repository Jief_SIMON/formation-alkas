<?php

include('minichatConnect.php');

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>MiniChat</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
        <script src='main.js'></script>
    </head>
    <body>
        <form action="minichat_post.php" method="POST">
            <label for="user">Username</label>
            <input type="text" name="user" id="user">
            <label for="message">Message</label>
            <textarea name="message" id="message" cols="30" rows="10"></textarea>
            <input type="submit" value="Send Message">
        </form>
        <section>
<?php

    $dbh = dbConnect();

    $messages = "SELECT * FROM messages ORDER BY id DESC LIMIT 0, 10";

    $stmt = $dbh->prepare($messages);

    $stmt->execute();

    $allMessages = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    foreach ($allMessages as $message) {
        
        echo sprintf(
            "
            <div>
            <span>
            <strong>
            %s :
            </strong>
            </span>
            <span>
            %s
            </span>
            <span>
            %s
            </span>
            </div>
            ",
            htmlspecialchars($message['user']),
            htmlspecialchars($message['message']),
            ($message['sent_at']),
        );
    }    

?>
        </section>
    </body>
</html>

