Un algorithme est un ensemble d'instructions visant à résoudre un problème.

En programmation, ces algorithmes sont définis à l'aide de symbôles qui constituent un langage de programmation.

Parmi ces symblôles, certains conceptes se retrouvent quelque soit le langage utilisé :

	1 - Variables :

Une variable est un registre qui permet de stocker et référencer une valeur.

Par exemple, si je définis une variable :

code_postal = 34000

Alors toute référence à code_postal renverra la valeur 34000.

Si je désire redéfinir une variable, je peux faire :

code_postal = 34090

Toute référence subséquente à code_postal renverra 34090.


	2 - Fonctions :

Une fonction est un ensemble d'instructions visant à effectuer une action précise. 
Ce bloc d'instructions peut être utilisé autant de fois que nécessaire en invoquant son nom.

En programmation, on déclare une fonction et son comportement dans un premier temps, pour ensuite l'invoquer ou l'exécuter plus tard.

	Exemple :

		Fonction Multiplier :
			pour deux nombres a et b 
    		renvoyer a * b


	3 - Boucles et Conditions :

Une boucle est une structure permettant de répéter le même jeu d'instructions plusieurs fois.
Une boucle possède une condition d'arrêt qui indique quand stopper l'exécution des instructions qu'elle contient.
Une boucle sans condition d'arrêt ou avec une condition d'arrêt mal définie est une boucle infinie.

Parmi les boucles existantes il en existe 2 très communes en programmation :
	- Tant que (while) : exécuter les actions jusqu'à ce qu'une condition soit validée.
	- Pour / Pour chaque (for / foreach) : exécuter les actions pour chaque éléments d'une suite.


	4 - Structures Conditionnelles :

Une structure conditionnelle permet d'effectuer un branchement (ou choix) entre deux jeux d'instructions selon une condition.
Ces structures se définissent à l'aide des termes Si (If) et Sinon (Else) :

	Si age_utlilisateur >= 18 Alors
		Donner accès au site
	Sinon
		Renvoyer chez Maman


	5 - Types de Données :

Un ordinateur ne connaissant que des valeurs représentées en binaire, il faut associer des valeurs binaires à d'autres concepts pour obtenir des données complexes, comme les lettres ou les sons.

La première structure de données utilisée est tout simplement le nombre entier (integer en anlais, int en programmation).

Viennent ensuite les nombres décimaux (à virgule flottante en informatique, float en programmation).

Les caractères quant à eux sont représentés également en binaire à l'aide de jeux de caractères qui font correspondre un nombre (en binaire) à un symbôle.
	Exemple : "a" en ASCII correspond à 97, ou 1100001 en binaire, ou 61 en héxadécimal.

Une chaîne de caractères (appelée string en anglais) est un ensemble de caractères.

Les booléens sont un type de données indiquant uniquement "vrai" ou "faux" (0 ou 1, true ou false).
Une condition par exemple s'évalue toutjours en booléen.


	6 - Structures de Données :

Une structure de données sert à organiser les données d'une certaine manière pour améliorer l'efficacité d'un programme, ou même son écriture.

Dans les structures de données courantes, on retrouve : 
	- les tableaux
	- les listes
	- les dictionnaires
	- les graphes

	- Tableaux :
Un tableau en programmation est une structure permettant de mémoriser plusieurs données de types semblables.
Par exemple, un tableau contenant les températures prévues de Montpellier contiendrait :

	  0    1    2    3    4    5    6    7
	[ 19 | 21 | 19 | 17 | 15 | 14 | 13 | 13 ] 

Un tableau identifie chaque case à l'aide d'un indice, partant de 0.
En demandant une valeur se trouvant à une certaine case, le programme pourra ainsi récupérer une valeur précise.

	