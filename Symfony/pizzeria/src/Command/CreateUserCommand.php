<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = "app:create-user";

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('username', InputArgument::REQUIRED, 'Username of the user to create');
        $this->addArgument('password', InputArgument::REQUIRED, 'Password of the user to create');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Create a new user',
            '================='
        ]);

        $user = new User();
        $user->setUsername($input->getArgument('username'));
        $user->setPassword(
            $this->encoder->encodePassword(
                $user,
                $input->getArgument('password')
            )
        );
        $user->setRoles(['ROLE_ADMIN']);

        $this->entityManager->persist($user);
        try {
            $this->entityManager->flush();
        } catch (Exception $e) {
            $output->writeln(
                'An error occured !'
            );
            return Command::FAILURE; 
        }

        $output->writeln(
            'User successfuly created !'
        );

        return Command::SUCCESS;
    }
}