<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class PictureModifyFormType extends AbstractType
{ 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('pictureFile', FileType::class, [
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                    ],
                    'mimeTypesMessage' => 'Only PNG and JPEG are allowed',
                    'maxSize' => '2048k',
                ])
            ],
        ])
        ->add('deletePicture', CheckboxType::class, [
            'label' => 'Revert picture to default',
            'required' => false,
            'mapped' => false
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Modify Picture'
        ]);
    }
}