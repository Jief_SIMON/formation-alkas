<?php

namespace App\Controller;

use App\Entity\User;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);

        $articles = $articleRepository->findAll();

        return $this->render(
            'main/index.html.twig',
            [
                "articles" => $articles
            ]);
    }

    /**
     * @Route("/article/{id}", name="view")
     */
    public function view(int $id): Response
    {
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);
        $article = $articleRepository->find($id);

        if (!$article) {
            throw $this->createNotFoundException("Article does not exist");
        }

        return $this->render(
            'main/view.html.twig', 
            [
                "article" => $article
            ]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function createArticle(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $article = new Article();
        
        $form = $this->createForm(ArticleType::class); 
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $user = $this->getUser();

            $article->setCreatedAt(new \DateTime());

            $article->setAuthor($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('view', ["id" => $article->getId()]);
        }

        return $this->render(
            "main/create.html.twig", 
            [
                "createForm" => $form->createView()
            ]
        );
    }

    /**
     * @Route("/update/{id}", name="update")
     */
    public function updateArticle(int $id, Request $request): Response
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException("Article does not exist");
        }

        if ($article->getAuthor()->getId() !== $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder($article)
            ->add('title')
            ->add('content', CKEditorType::class, [
                'config' => [
                    'filebrowserBrowseRouteParameters' => [
                        'instance' => 'default',
                        'homeFolder' => $this->getUser()->getUsername()
                    ]
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Update Article'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('view', ['id' => $article->getId()]);
        }

        return $this->render(
            "main/update.html.twig", 
            [
                "updateForm" => $form->createView()
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteArticle(int $id): Response
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException("Article does not exist");
        }

        if ($article->getAuthor()->getId() !== $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/category/{name}", name="articles_by_category")
     */
    public function articleByCategory(Category $category) 
    {
        return $this->render("index.thml.twig", [
            "articles" => $category->getArticles()
        ]);
    }
}
