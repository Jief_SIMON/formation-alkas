<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $pictureFile = $form->get('pictureFile')->getData();

            if ($pictureFile) {
                $fileName = md5(uniqId(rand())) . "." . $pictureFile->guessExtension();
                $fileDestination = $this->getParameter('user_profile_pictures_dir');
 
                try {
                    $pictureFile->move($fileDestination, $fileName);
                } catch (FileException $e) {
                    throw new HttpException(500, 'An error occured during file upload');
                }

            } else {
                $fileName = 'default.png';
            }
            
            $user->setPicture($fileName);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'registration/register.html.twig', 
            [
                'registrationForm' => $form->createView(),
            ]
        );
    }
}
