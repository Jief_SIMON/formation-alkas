<?php

namespace App\Controller;

use App\Form\PasswordModifyFormType;
use App\Form\PictureModifyFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="profile_index")
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        return $this->render('profile/index.html.twig');
    }

    /**
     * @Route("/view/{username}", name="profile_view")
     */
    public function view(string $username, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $username]);

        if (!$user) {
            $this->createNotFoundException("No user found with username" . $username);
        }
        return $this->render(
            'profile/view.html.twig',
            [
                "user" => $user
            ]
        );
    }

    /**
     * @Route("/password-modify", name="profile_password_modify")
     */
    public function modifyPassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();

        $form = $this->createForm(PasswordModifyFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword(
                $user, 
                $form->get('newPassword')->getData()
                )
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('profile_index');
        }
        
        return $this->render(
            'profile/modifyPassword.html.twig',
            [
                "passwordModifyForm" => $form->createView()
            ]
        );

    }

    /**
     * @Route("/picture-modify", name="profile_picture_modify")
     */
    public function modifyPicture(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();

        $form = $this->createForm(PictureModifyFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pictureFile = $form->get('pictureFile')->getData();
            $deletePicture = $form->get('deletePicture')->getData();

            if ($pictureFile && !$deletePicture) {
                $fileName = md5(uniqId(rand())) . "." . $pictureFile->guessExtension();
                $fileDestination = $this->getParameter('user_profile_picture_dir');
                
                try {
                    $pictureFile->move($fileDestination, $fileName);
                } catch (FileException $e) {
                    throw new HttpException(500, 'An error occured during file upload');
                }
                if ($user->getPicture() !== 'default.png') {
                    $this->removeProfilePicture($user->getPicture());
                }
                $user->setPicture($fileName);
                // do anything else you need here, like send an email
            }
            
            if ($deletePicture) {
                $this->removeProfilePicture($user->getPicture());
                
                $user->setPicture('default.png');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('profile_index');
        }

        return $this->render(
            'profile/modifyPicture.html.twig',
            [
                "modifyPictureForm" => $form->createView()
            ]
        );
    }

    public function removeProfilePicture(string $path)
    {
        $fs = new Filesystem();
        try {
            $fs->remove($this->getParameter('user_profile_pictures_dir') . '/' . $path);
        } catch (IOException $e) {
            throw new HttpException(500, 'An error occured during file upload');
        }
    }
}
