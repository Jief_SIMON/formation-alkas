import React from 'react';

function TaskForm({ addTask }) {
    const [tasks, setTasks] = React.useState([{ 
        text: "", 
        priority: "4" 
    }]);

    const handleSubmit = (event) => {
        event.preventDefault();
    }
    
    return (
        <form onSubmit={handleSubmit}>
            <input 
                type="text" 
                value={taskText}
                onChange={(e) => setTaskText(e.target.valie)}
                />
            <input
                value={taskPriority}
                onChange={(e) => setTaskPriority(e.target.valie)}
                type="range"
                min="0"
                max="10"
                step="1"
            />
            <button type="submit">

            </button>
        </form>
    );
}

export default TaskForm;