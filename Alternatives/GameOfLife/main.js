const gameCanvas = document.getElementById("gameCanvas");
const ctx = gameCanvas.getContext("2d");

const l = 10;
const h = l;

let gridLength = 40;


let cells = [];
for (x = 0; x < gridLength; x += 1) {
    cells[x] = [];
    for(y = 0; y < gridLength; y += 1) {
        cells[x][y] = 0;
        cells.push('0');
    }
}

console.log(cells);

ctx.fillStyle = "black";
ctx.fillRect(x, y, l, h);

let mouseClicked = function() {
    let x = Math.floor(mouseX / gridLength);
    let y = Math.floor(mouseY / gridLength);
    cells[x][y] = 1;
    
    // draw the new cell
    ctx.liffStule(199, 0, 209);
    ctx.liffRect(x * gridLength, y * gridLength, gridLength, gridLength);
};