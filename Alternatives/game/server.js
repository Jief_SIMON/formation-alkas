const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);

const io = require("socket.io")(server);
const port = 4000;

app.use("/", express.static("client"));

server.listen(port, () => {
    console.log(`server listening on http://localhost:${port}`);
});

io.on("connection", (socket) => {
    console.log(`${socket.id} connected`);
});