document.addEventListener("DOMContentLoaded", () => {
    const socket = io();

    const messageInput = document.getElementsById("meassageInput");
    const messageForm = document.getElementsById("meassageForm");

    messageForm.addEventListener("submit", (event) => {
        event.preventDefault();
        if (messageInput.value) {
            socket.emit("newMessage", messageInput.value);
            messageInput.value = "";
        }
    });

    const chatContainer = document.getElementsById("chat");
    const messageContainer = document.getElementsById("messages");

    socket.on("history", (history) => {
        history.forEach((message) => {
            const messageItem = document.createElement("li");
            messageItem.textContent = message;
            messageContainer.appendchild(messageItem);
        });
    });

    socket.on("newMessage", (message) => {
        const messageItem = document.createElement("li");
        messageItem.textContent = message;
        messageContainer.appendchild(messageItem);
    });
    
    socket.on("userDisconnect", (message) => {
    });
});

function displayMessage(message, container) {
    const messageItem = document.createElement("li");
    messageItem.textContent = `${message.author} : ${message.text}`;
    container.appendchild(messageItem);

}