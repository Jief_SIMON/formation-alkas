const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const io = require("socket.io")(server);

const port = process.env.PORT || 4000;
server.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

// app.get("/", (req, res) => {
//     res.sendFile(__dirname + "/client/index.html");
// });

app.use(express.static(__dirname + "/client"));

const history = [];

io.on('connection', (socket) => {
    console.log(`connected on ${socket.id}`);

    socket.emit("history", history);

    socket.on("disconnected", () => {
        console.log(`${socket.id} disconnected `);
        io.emit("userDisconnect", `${socked.id} disconnected`);
    });

    socket.on("newMessage", (message) => {
        console.log(`${socket.io} : ${message}`);
        const newMessage = {
            text: message,
            author: socket.id,
        }
        history.push(newMessage);
        io.emit("newMessage", newMessage);
    });
})