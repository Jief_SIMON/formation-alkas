document.addEventListener("DOMContentLoaded", () => {
    const openBtn = document.getElementById("open-button");
    const nav = document.querySelector("nav");
    const main = document.querySelector("main");

    openBtn.addEventListener("click", () => {
        nav.classList.toggle("translated");
        main.classList.toggle("perspective");
    });
});
