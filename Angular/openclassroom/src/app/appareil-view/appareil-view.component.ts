import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {
    isAuth = false;

    lastUpdate = new Promise(
        (resolve, reject) => {
            const date = new Date();
            setTimeout(() => {
                resolve(date);
            }, 2000);
        }
    );

    appareils: any[];
    appareilSubscription: Subscription;

    appareilOff = "éteint";
    appareilOn = "allumé";

    constructor(private appareilService: AppareilService) {
        setTimeout(
            () => {
                this.isAuth = true;
            }, 4000
        );
    }

    ngOnInit() {
        this.appareilSubscription = this.appareilService.appareilSubject.subscribe(
            (appareils: any[]) => {
                this.appareils = appareils;
            }
        );
        this.appareilService.emitAppareilSubject();
    }

    onAllumer() {
        this.appareilService.switchOnAll();
        console.log('On allume tout !');
    }

    onEteindre() {
        if (confirm("Etes-vous sûr de vouloir éteindre tous vos appareils ?")) {
            this.appareilService.switchOffAll();
            console.log('On éteind tout !');
        } else {
            return null;
        }
    }
}
