import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Hero } from './hero';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) { }

    private apiUrl = 'api/heroes';

    getHeroes(): Observable<Hero[]> {
        return (
            this.http
                .get<Hero[]>(this.apiUrl)
                .pipe(
                    tap((_) => 
                        this.log(
                            'fetched heroes list'
                        )
                    )
                )
        );
    }

    getTopHeroes(): Observable<Hero[]> {
        return this.getHeroes().pipe(
            map( (heroes) => {
                this.log('fetched top heroes');
                return heroes
                    .sort((a: Hero, b: Hero) => b.rating - a.rating)
                    .slice(0, 3);
            })
        );
    }

    getHero(id: number): Observable<Hero> {
        return this.http.get<Hero>(`${this.apiUrl}/${id}` ).pipe(
            tap((hero) => {
                this.log(
                    `fetched hero #${hero.id}`
                );
            })
        );
    }

    add(hero: Hero): Observable<Hero> {
        return this.http
            .post<Hero>(this.apiUrl, hero)
            .pipe(
                tap((hero) => this.log(`hero ${hero.id} added`))
        );
    }

    delete(hero: Hero): Observable<Hero> {
        return this.http
            .delete<Hero>(`${this.apiUrl}/${hero.id}`)
            .pipe(
                tap((_) => this.log(`hero ${hero.id} deleted`))
        );
    }

    update(hero: Hero): Observable<Hero> {
        return this.http
            .put<Hero>(`${this.apiUrl}/${hero.id}`, hero)
            .pipe(
                tap((_) => this.log(`hero ${hero.id} updated`))
        );
    }

    log(message: string) {
        this.messageService.add(`HeroService : ${message}`);
    }

    searchHeroes(term: string): Observable<Hero[]> {
        if (!term.trim()) {
            return of([]);
        }
        return this.http.get<Hero[]>(`${this.apiUrl}/?name=${term}`).pipe(
            tap((heroes) => this.log(`search: ${heroes.length} hero(es) found`))
        );
    }
}
