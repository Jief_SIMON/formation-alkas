import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    private searchTerms = new Subject<string>();

    // Par convention, on rajoute un $ à la fin d'un nom de variable contenant un observable.
    heroes$: Observable<Hero[]>

    constructor(private heroService: HeroService) { }

    ngOnInit(): void {
        this.heroes$ = this.searchTerms.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            switchMap((term: string) => this.heroService.searchHeroes(term))
        );
    }

    search(term: string): void {
        this.searchTerms.next(term);
    }

}
