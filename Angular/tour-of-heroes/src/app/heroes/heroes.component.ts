import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  
    heroes: Hero[] = [];

    constructor(private heroService: HeroService) {}
    
    ngOnInit(): void {
        this.getHeroes();
    }

    getHeroes(): void {
        this.heroService
        .getHeroes()
        .subscribe((heroesData) => (this.heroes = heroesData));
    }

    add(name: string, rating: number):void {
        if (!name) {
            return;
        }
        const hero = {
            name: name,
            rating: rating,
        }
        this.heroService
            .add(hero as Hero)
            .subscribe((hero) => this.heroes.push(hero));
    }

    delete(hero: Hero): void {
        this.heroService
            .delete(hero)
            .subscribe(
                () => (this.heroes = this.heroes.filter((h) => h !== hero))
            );
    }
}
