import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
    constructor(
        private authService: AuthService,
        private fb: FormBuilder,
        private router: Router
    ) {}

    formLogin?: FormGroup;

    ngOnInit(): void {
        this.formLogin = this.initForm();
    }

    auth(username: string, password: string) {
        this.authService.auth(username, password).subscribe(
            (res) => {
                //on redirige si tout va bien
                this.router.navigateByUrl('/genre');
            },
            (err) => {
                console.error('wrong username or password');
            }
        );
    }

    login() {
        const loginValues = this.formLogin!.value;
        this.auth(loginValues.username, loginValues.password);
    }

    initForm() {
        return this.fb.group({
            username: [
                '',
                [Validators.required, Validators.pattern('(?!^ +$)^.+$')],
            ],
            password: [
                '',
                [Validators.required, Validators.pattern('(?!^ +$)^.+$')],
            ],
        });
    }

    get username() {
        return this.formLogin!.get('username')!;
    }
    get password() {
        return this.formLogin!.get('password')!;
    }
}
