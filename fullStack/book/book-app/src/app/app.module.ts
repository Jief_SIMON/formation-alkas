import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GenreListComponent } from './genre/genre-list/genre-list.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GenreCreateComponent } from './genre/genre-create/genre-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GenreDetailComponent } from './genre/genre-detail/genre-detail.component';
import { AutofocusDirective } from './directive/autofocus.directive';
import { AuthComponent } from './auth/auth.component';

@NgModule({
    declarations: [
        AppComponent,
        GenreListComponent,
        GenreCreateComponent,
        GenreDetailComponent,
        AutofocusDirective,
        AuthComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    providers: [
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
