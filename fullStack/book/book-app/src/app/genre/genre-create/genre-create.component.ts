import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Genre } from 'src/app/model/genre';
import { GenreService } from 'src/app/service/genre.service';

@Component({
    selector: 'app-genre-create',
    templateUrl: './genre-create.component.html',
    styleUrls: ['./genre-create.component.css']
})
export class GenreCreateComponent implements OnInit {

    @Output() onCreate = new EventEmitter<Genre>();

    constructor(private fb: FormBuilder, private gs: GenreService) {}

    createForm!: FormGroup;

    ngOnInit(): void {
        this.createForm = this.fb.group(
            {
                name: [
                    '',
                    [
                        Validators.required,
                        Validators.maxLength(30),
                        Validators.pattern('(?!^ +$)^.+$'),
                    ],
                ],
            }
        );
    }

    get formName() {
        return this.createForm.get('name')!;
    }

    create(): void {
        const genre: Genre = this.createForm.value;
        genre.name = genre.name.trim();

        this.createForm.get('name')!.reset();
        this.gs
            .create(genre)
            .subscribe((res) => this.onCreate.emit(res.created));
    }
}
