import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(private http: HttpClient) {}

    private loginUrl = 'http://localhost:8000/login_check';

    public auth(username: string, password: string): Observable<any> {
        const body = {
            username: username,
            password: password,
        };
        return this.http.post<any>(this.loginUrl, body).pipe(
            tap((res) => {
                //on constitue un objet pour stocker et le token et sa charge utile contenant les informations de connexion
                const jwt = {
                    token: res.token,
                    payload: this.getTokenPayload(res.token),
                };
                //pour pouvoir le stocker dans le localstorage il faut le retransformer en string au format JSON
                localStorage.setItem('jwt', JSON.stringify(jwt));
            })
        );
    }

    public logout() {
        localStorage.removeItem('jwt');
    }

    public getToken() {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const parsed = JSON.parse(jwt);
            //on vérifie si le token a expiré en comparant la propriété expiration de notre payload du jwt
            if (parsed.payload.exp < Date.now() / 1000) {
                console.log('jwt expiré');
                localStorage.removeItem('jwt');
                //en renvoyant null, on permet a l'authInterceptor d'ignorer le token
                return null;
            }
            return parsed;
        }
        return jwt;
    }

    public getTokenPayload(token: string) {
        return JSON.parse(atob(token.split('.')[1]));
    }
}
