import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Created } from '../model/created';
import { Genre } from '../model/genre';

@Injectable({
    providedIn: 'root',
})
export class GenreService {
    constructor(private http: HttpClient) {}

    private apiUrl: string = 'http://localhost:8000/genre';

    getAll(): Observable<Genre[]> {
        return this.http.get<Genre[]>(this.apiUrl);
    }

    create(genre: Genre): Observable<Created<Genre>> {
        return this.http.post<Created<Genre>>(this.apiUrl, genre);
    }

    get(id: number): Observable<Genre> {
        return this.http.get<Genre>(`${this.apiUrl}/${id}`);
    }

    update(genre: Genre): Observable<Genre> {
        return this.http.put<Genre>(`${this.apiUrl}/${genre.id}`, genre);
    }

    delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.apiUrl}/${id}`);
    }
}
