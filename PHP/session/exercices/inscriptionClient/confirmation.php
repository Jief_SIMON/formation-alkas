<?php

// On vérifie l'intégrité de nos données.
if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['email']) && isset($_POST['zip']) && isset($_POST['city'])) {
    // Si tout est ok, on commence notre session.
    session_start();
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $zip = $_POST['zip'];
    $city = $_POST['city'];

    if (is_string($firstName) && is_string($lastName) && is_string($city) && is_string($zip)) {
        if (is_string($email)) {
            // Puis on enregistre les données de notre utilisateur dans notre stockage de session.
            // En ce faisant, on rend disponible les données quelque soit la page de notre site.
            $_SESSION['info'] = ['firstName' => $firstName, 'lastName' => $lastName, 'zip' => $zip, 'city' => $city];

            // On affiche ensuite notre récapitulatif,
            echo "Recap : <br/>";
            echo "Nous confirmons votre inscription, M/Mme " . $lastName . " " . $firstName . ".<br/>";
            echo " Votre adresse email est " . $email . ".<br/>";
            echo " Votre code postal  et votre ville de résidence sont " . $zip . ", " . $city . ".<br/>";

            // et une proposition de retour à l'accueil.
            echo "<br/> <a href='form.php'>Retour à l'accueil.</a>";

        } else {
            echo "Veuillez entrer une adresse mail valide.";
        }
    } else {
        echo "Les champs de texte doit uniquement contenir des caractères.";
    }
} else {
    echo "Erreur dans le formulaire !";
}