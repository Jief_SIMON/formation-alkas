<?php
    // On démarre notre session avant d'écrire du texte sinon session_start() ne fonctionne pas.
    session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <title>Exercice session</title>
</head>
<body>

    <?php
        // Si on a bien quelque chose dans notre stockage de session,
        if (isset($_SESSION['info'])) {
            // on affiche notre message,
            echo "Bienvenue, {$_SESSION['info']['firstName']} {$_SESSION['info']['lastName']}, vous êtes bien connecté sur la plateforme de {$_SESSION['info']['zip']}, {$_SESSION['info']['city']}";
        } else { // on affiche le formulaire.
    ?>
        <form method="POST" action="confirmation.php">
            <label for="firstName">Nom</label>
            <input type="text" name="firstName" id="firstName" placeholder="Nom">
            <label for="lastName">Prenom</label>
            <input type="text" name="lastName" id="lastName" placeholder="Prénom">
            <label for="email">E-mail</label>
            <input type="email" name="email" id="email" placeholder="Votre e-mail">
            <label for="zip">Code postal</label>
            <input type="text" name="zip" id="zip" placeholder="Code postal">
            <label for="city">Ville</label>
            <input type="text" name="city" id="city" placeholder="Ville">

            <input type="submit" value="Send">
        </form>
    <?php
        }
        if (isset($_SESSION['info'])) {
            // Toujours dans le cas où notre session serait remplie, on propose de la détruire.
            ?>
                <a href="destroy.php">Logout</a>
                <?php
        }
    ?>

</body>
</html>