<?php

class Personnage {

    private $_degats;
    private $_experience;
    private $_force;

    public function __construct($degats, $experience, $force) {
        $this->_degats = $degats;
        $this->_experience = $experience;
        $this->_force = $force;
    }

    public function frapper($perso) {
        $perso->_degats += $this->_force;
    }

    public function gagnerExperience() {
        $this->_experience += 1;
    }

    public function getDegats() {
        return $this->_degats;
    }

    public function getExperience() {
        return $this->_experience;
    }

    public function getForce() {
        return $this->_force;
    }

    public function setDegats($degats) {
        if (!is_int($degats)) {
            echo "La valeur doit être un entier";
        } else {
            $this->_degats = $degats;
        }
    }

    public function setExperience($experience) {
        if (!is_int($experience)) {
            echo "La valeur doit être un entier";
        } else {
            $this->_experience = $experience;
        }
    }

    public function setForce($force) {
        if (!is_int($force)) {
            echo "La valeur doit être un entier";
        } else {
            $this->_force = $force;
        }
    }
}

$perso1 = new Personnage(20, 10, 30);
$perso2 = new Personnage(30, 10, 20);

$perso1->setForce(50);
$perso1->setExperience(80);

$perso2->setForce(20);
$perso2->setExperience(30);

$perso1->frapper($perso2);
$perso1->gagnerExperience();

$perso2->frapper($perso1);
$perso2->gagnerExperience();

echo "<br/>La force du personnage 1 est de " . $perso1->getForce() . ". Il a " . $perso1->getDegats() . " points de dégâts. <br/>Ses points d'expérience ont un total de " . $perso1->getExperience() . " points.";

echo "<br/>La force du personnage 1 est de " . $perso2->getForce() . ". Il a " . $perso2->getDegats() . " points de dégâts. <br/>Ses points d'expérience ont un total de " . $perso2->getExperience() . " points.";