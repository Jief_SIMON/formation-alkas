<?php
// Pour se connecter à une base de données en PHP,
// il faut qu'on utilise une classe d'objets appelé PDO (PHP Data Objects).
// La classe PDO contient des méthodes permettant de dialoguer avec une BDD.
// Ici, on va contacter une serveur de bdd mysql et demander l'accès à une base de données test.

// Pour se connecter à la base, on va instancier un PDO :
// Le premier paramètre du constructeur PDO est le DSN (Data Source Name).
// Il sert à identifier à quelle base de données on souhaite se connecter.
// Il s'écrit au format : $driver:host=$nomhote;dbname=$nombdd;charset=utf8.
// Dans mysql, le driver est mysql par exemple,

$dbh = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
// On appelle notre objet PDO 'dbh' pour DataBase Handler ou gestionnaire de bdd.

// Pour preparer nos requêtes SQL, on peut les définir comme des chaînes de caractères étant donné que mySQL dialogue via du texte;
// pour créer une table par exemple :
// $sql_query = "CREATE TABLE test_php (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, label VARCHAR(255))";
$sql_query = "SELECT * FROM test_php";
// Pour que la requête SQL en format texte soit envoyé à mySQL, il faut qu'on prépare un statement (une déclaration) que PDO se chargera d'envoyer au DSN précisé plus haut.
// prepare permet de préparer la déclaration à SQL via PDO, et renvoie la déclaration.
$stmt = $dbh->prepare($sql_query);
// Enfin, il faut éxécuter la déclaration obtenue :
$stmt->execute();
// Si une erreur survient, on peut l'afficher avec $stmt->errorInfo() qui est un tableau contenant les erreurs survenues.
// var_dump($stmt->errorInfo());

// Si on attend une réponse à notre requête, il faut demander à PDO d'aller la chercher et nous la renvoyer. Pour ça, il faut utiliser une commande fetch (pour 'aller chercher').
// fetchAll renvoie toutes les lignes de résultat, et le renvoie par défaut sous deux formes : une forme sous tableau classique (index 0 - n), et une forme clé/valeur en tableau associatif.
// Pour ne choisir qu'une seule forme de résultat, on peut lui préciser comme par exemple avec PDO::FETCH_ASSOC qui indique d'utiliser un tableau associatif.
$res = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Pour chaque ligne de résultat,
foreach($res as $ligne){
    // on affiche l'id ou le label.
    echo sprintf("id : %d, label : %s <br/>", $ligne['id'], $ligne['label']);
}