<?php

session_start();

if (isset($_SESSION['userid'])) {
    header('Location: index.php');
}

$page_title = "Connexion";
include('head.php');

?>
<body>

    <form action="connexion.php" method="POST">
        <fieldset>
            <legend>
                Connexion
            </legend>
            <label for="nickname">Pseudo : </label>
            <input type="text" name="nickname" id="nickname">
            <label for="password">Mot de passe : </label>
            <input type="text" name="password" id="password">
            <input type="submit" value="Connexion">
        </fieldset>
        <fieldset>
            <legend>
                Incription
            </legend>
            <label>Si vous n'avez pas de compte,</label>
            <a href="inscription_form.php"><button type="button" id="inscription-btn">inscrivez-vous</button></a>
        </fieldset>
    </form>
    
</body>
</html>