<nav>
    <ul>
    <?php
    if (isset($_SESSION['userid'])) {
    ?>    
        <li><a href="index.php">Accueil</a></li>
        <li><a href="redigerArticle.php">Rédiger article</a></li>
        <li><a href="deconnexion.php">Déconnexion</a></li>
    <?php
    } else {
    ?>
        <li><a href="index.php">Accueil</a></li>
        <li><a href="connexion_form.php">Connexion</a></li>
        <li><a href="inscription_form.php">Inscription</a></li>
    <?php
    }
    ?>
    </ul>
</nav>