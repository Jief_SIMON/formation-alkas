<?php

require_once('blogConnect.php');

if (isset($_POST['nickname']) && isset($_POST['password'])) {
    $_POST['nickname'] = $nickname;
    $_POST['password'] = $password;

    if (!empty($nickname) && !empty($password)) {

        $dbh = dbConnect();

        $get_user = "SELECT * FROM user WHERE nickname = :nickname";
        $stmt = $dbh->prepare($get_user);

        $stmt->execute([
            ":nickname" => $nickname,
        ]);

        $user = $stmt->fetch(PDO::FETCH_OBJ);

        if ($user) {
            if (password_verify($password, $user["password"])) {
                session_start();

                $_SESSION["userid"] = $user["id"];
                
                header('Location: index.php');
            
            } else {
                echo "Nom d'utilisateur ou Mot de pass incorrectes";
            }
        } else {
            echo "Nom d'utilisateur ou Mot de pass incorrectes";
        }
    } else {
        echo "Erreur valeur vide";
    }
} else {
    echo "Erreur dans le formulaire";
}