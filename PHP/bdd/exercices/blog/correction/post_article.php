<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Post New Article</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <form action="post_article_process.php" method="POST">
        <label for="title">
            Title
        </label>
        <input type="text" name="title" id="title"> 
        <label for="content">
            Content
        </label>
        <textarea type="text" name="content" id="content"></textarea>
        <input type="submit" value="Post Article ">
    </form>
</body>
</html>