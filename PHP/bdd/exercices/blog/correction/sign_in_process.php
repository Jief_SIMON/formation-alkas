<?php

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (!empty($username) && !empty($password)) {
        include ("blogConnect.php");
        $dbh = dbConnect();

        $stmt = $dbh->prepare("SELECT * FROM user WHERE username = :username");

        $stmt->execute(
            [
                ":username" => $username
            ]
        );

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            if (password_verify($password, $user["password"])) {
                session_start();
                $_SESSION["userid"] = $user.['id'];
                header('Location: index.php');
            } else {
                echo "ERREUR";
                // header('Location: sign_in.php');
            }
        } else {
            echo "ERREUR";
            // header('Location: sign_in.php');
        }
    }
}