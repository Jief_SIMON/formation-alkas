<?php

require_once('user.php');

class BlogComment
{
    public int $id;
    public BlogUser $author;
    public BlogArticle $article;
    public string $content;
    public DateTime $created_at;

    public function __construct(int $id, BlogUser $author, BlogArticle $article, string $content, DateTime $created_at) 
    {
        $this->id = $id;
        $this->author = $author;
        $this->article = $article;
        $this->content = $content;
        $this->created_at = $created_at;
    }
}