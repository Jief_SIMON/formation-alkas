<?php

/**
 * Contient la classe DatabaseHandler permettant de dialoguer avecx la base de données via des méthodes
 */

 // On récupère nos modèles de données permettant de ranger proprement les données de la base dans des objets.
 require_once('model/article.php');
 require_once('model/user.php');

 /**
  * Raccourci pour instancier notre databasehandler sur notyre bdd blog
  */

function getDatabaseHandler() 
{
    return new DatabaseHandler("blog", "blog_user", "Qq1YMyU5af8tDZyh");
}

/**
 * Permet l'utilisation de méthodes de dialogue avec la base de données
 */
class DatabaseHandler
{
    private string $_dbname;
    private string $_username;
    private string $_password;

    //Contient l'instance de PDO permattant la communication avec la base de données
    private PDO $_handler;

    public function __construct(string $dbname, string $username, string $password)
    {
        $this->_dbname =  $dbname;
        $this->_username = $username;
        $this->_password = $password;

        $this->connect();
    }

    /**
     * Permet la connexion à la base de données en instanciant un objet PDO
     * 
     * L'objet PDO instancié est rangé dans la propriété _handler du DatabaseHandler
     */
    Protected function connect()
    {
        try {
            $dbh = new PDO("mysql:host=localhost;charset=utf8; dbname={$this->_dbname}", $this->_username, $this->_password);
        } catch (PDOException $e) {
            http_response_code(500);
            die("500 - Internal Server Error");
        }

        $this->_handler = $dbh;
    }

    /**
     * Renvoie l'instance de PDO utilisé par DatabaseHandler
     */
    public function getHandler()
    {
        return $this->_handler;
    }


    //GESTION DES ARTICLES

    /**
     * Renvoie tous les articles de la base de données
     * 
     * @return BlogArticle[]
     */
    public function getArticles()
    {
        $stmt = $this->_handler->prepare("SELECT * FROM article ORDER BY created_at");
        $stmt->execute();

        $articles = [];
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Pour chaque ligne de résultats
        foreach ($res as $article) {
            //On range dans un tableau une nouvelle instance de BlogArticle
            //Chaque BlogArticle représente un article
            $author = $this->getUserById($article['author']);
            array_push($articles, new BlogArticle($article['id'], $author, $article['title'], $article['content'], new DateTime($article['created_at'])));
        }
        //on renvoie nos BlogArticle
        return $articles;
    }

    /**
     * Renvoie un article de type BlogArticle
     * @param int $id l'id de l'article
     * @return ?BlogArticle
     */
    public function getArticle(int $id): ?BlogArticle
    {
        $stmt = $this->_handler->prepare("SELECT * FROM article WHERE id = :id");
        $stmt->execute(
            [
                ":id" => $id,
            ]
        );

        $article = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($article) {
            //on récupère l'auteur de l'article
            //étant donné que notre base de donnée ne permet pas un article sans author
            //on a pas besoin de vérifier si l'auteur a bien été récupéré, il le sera sans problème si l'article existe.
            $author = $this->getUserbyId($article['author']);

            //on renvoie une instance de BlogArticle si on reçoit un résultat
            return new BlogArticle($article['id'], $author, $article['title'], $article['content'], new DateTime($article['Created_at']));
        } else {
            return null;
        }
    }

    /**
     * Insère un article dans notre table article
     */
    public function createArticle(string $title, string $content, DateTime $created_at, BlogUser $author)
    {
        $stmt = $this->_handler->prepare("INSERT INTO article (title, content, created_at, author) VALUES (:title, :content, :created_at, :author)");
        //Date de création de l'article
        $date = $created_at->format("Y-m-d H:i:s");
        $stmt->execute(
            [
                ":title" => $title,
                ":content" => $content,
                ":created_at" => $date,
                ":author" => $author->id,
            ]
        );
    }

    /**
     * Permet de modifier un article dans la base de données
     */
    public function updateArticle(int $id, string $title, string $content)
    {
        $stmt = $this->_handler->prepare("UPDATE article SET title = :title, content = :content WHERE id = :id");
        $stmt->execute(
            [
                ":title" => $title,
                ":content" => $content,
                ":id" => $id,
            ]
        );
    }

    /**
     * Supprime un article selon son id
     */
    public function deleteArticle(int $id, string $title, string $content)
    {
        $stmt = $this->_handler->prepare("DELETE FROM article WHERE id = :id");
        $stmt->execute(
            [
                ":id" => $id,
            ]
        );
    }

    
    //GESTION DES COMMENTAIRES

    public function createComment(string $content, DateTime $created_at, BlogUser $author, BlogArticle $article)
    {
        $stmt = $this->_handler->prepare("INSERT INTO comment (content, created_at, author, article) VALUES (:content, :created_at, :author, :article)");

        $date = $created_at->format('Y-m-d H:i:s');
        $stmt->execute(
            [
                ":content" => $content,
                ":created_at" => $date,
                ":author" => $author->id,
                ":article" => $article->id,
            ]
        );
    }

    public function getCommentsByArticle(BlogArticle $article)
    {
        $stmt = $this->_handler->prepare("SELECT * FROM comment WHERE article = :article ORDER BY created_at");
        $stmt->execute(
            [
                ":article" => $article,
            ]
        );

        $comments = [];
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        //Pour chaque ligne de résultat
        foreach ($res as $comment) {
            //On range dans un tableau une nouvelle instance de BlogComment
            //chaque BlogComment représente un comment

            //On récupère l'objet user auteur du commentaire
            $author = $this->getUserById($comment['author']);
            array_push($comments, new BlogComment($comment['id'], $comment['content'], $article, $author, new DateTime($comment['created_at'])));
        }
        //On renvoie nos BlogComment
        return $comments;
    }

    /**
     * Récupération d'un commentaire par son id
     */
    public function getComment(int $id)
    {
        $stmt = $this->_handler->prepare("SELECT * FROM comment WHERE id = :id");

        $stmt->execute(
            [
                ":id" => $id,
            ]
        );

        $comment = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($comment) {
            $author = $this->getUserById($comment['author']);
            $article = $this->getArticle($comment['article']);
            return new BlogComment($comment['id'], $author, $article, $comment['content'], new DateTime($comment['created_at']));
        } else {
            return null;
        }
    }

    public function updateComment()
    {

    }

    public function deleteComment()
    {
        
    }


    //GESTION DES UTILISATEURS

    /**
     * Renvoie un utilisateur selon son id, null sinon
     * 
     * @return ?BlogUser
     */
    public function getUserById(int $id): ?BlogUser
    {
        $stmt = $this->_handler->prepare("SELECT * FROM user WHERE id = :id");
        $stmt->execute(
            [
                ":id" => $id,
            ]
        );

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            //Si l'utilisateur a été trouvé, on renvoie une instance de BlogUser
            return new BlogUser($user['id'], $user['username'], $user['password']);
        } else {
            return null;
        }
    }

    /**
     * Renvoie un utilisateur selon son 'username'
     * 
     * @return ?BlogUser
     */
    public function getUserByUsername(string $username): ?BlogUser
    {
        $stmt = $this->_handler->prepare("SELECT * FROM user WHERE username = :username");
        $stmt->execute(
            [
                ":username" => $username,
            ]
        );

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            return new BlogUser($user['id'], $user['username'], $user['password']);
        } else {
            return null;
        }
    }

    /**
     * Crée un nouvel utilisateur
     * 
     * Renvoie un 'Exception' en cas de duplicata
     * 
     * @throws Exception
     */
    public function createUser($username, $password_hash)
    {
        $stmt = $this->_handler->prepare("INSERT INTO user (username, password) VALUES (:username, :password)");
        $stmt->execute(
            [
                ":username" => $username,
                ":password" => $password_hash,
            ]
        );

        $errors = $stmt->errorInfo();

        if ($errors[0] == "23000" && $errors[1] == 1062) {
            throw new Exception("User already exists");
        }
    }
}