<?php

//Ici, on regroupera toutes les fonctions qui permettront d'afficher du HTML dans notre blog

/**
 * Affiche l'en-tête html de la page
 * @param string $title le titre de la page
 */
function displayHeader(string $title)
{
    echo "
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>{$title}</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
        </head>
        <body>
            <div class='container'>
    ";
}

/**
 * Affiche le pied de page html de la page
 */
function displayFooter()
{
    echo "
        </div>
        </body>
        </html>
    ";
}

/**
 * Affiche le menu de navigation
 * @param User $user l'utilisateur connécté
 */
function displayNav($user = null)
{
    if ($user) {
        $navlinks = "
            <li><a href='post_article.php'>New Article</a></li>
            <li><a href='sign_out.php'>Sign Out</a></li>";
    } else {
        $navlinks = "
        <li><a href='sign_in.php'>Sign In</a></li>
        <li><a href='sign_up.php'>Sign Up</a></li>";
    }

    echo "
        <nav>
            <ul>
                <li><a href='index.php'>Home</a></li>
                {$navlinks}
            </ul>
        </nav>
    ";
}

/**
 * Affiche la représentation html d'un article
 */
function displayArticle(BlogArticle $article, ?BlogUser $user)
{
    //Affiche des actions dans le footer
    if ($user && $user->id == $article->author->id) {
        $footer = "<a class='action update' href='update_article.php?id={$article->id}'>Update</a>
        <a class='action delete' href='delete_update.php?id={$article->id}'>Delete</a>";
    } else {
        $footer = "";
    }

    $article_content = nl2br($article->content);

    echo "
        <article>
            <header>
                <h2>{$article->title}</h2>
                <h3>{$article->author}</h3>
                <h4>{$article->created_at->format('Y-m-d H:i:s')}</h4>
            </header>
            <section>
                <p>{$article_content}</p>
            </section>
            <footer>
                {$footer}
            </footer>
        </article>
    ";
}

function displayArticles(array $articles, ?BlogUser $user)
{

    foreach ($articles as $article) {
        //Affiche des actions dans le footer
        if ($user && $user->id == $article->author->id) {
            $footer = "<a class='action update' href='update_article.php?id={$article->id}'>Update</a>
            <a class='action delete' href='delete_update.php?id={$article->id}'>Delete</a>";
        } else {
            $footer = "";
        }
    
        // On coupe notre contenu en mots avec explode, on récupère les n premiers mots, et on colle le tout avec des espaces
        // $truncated = implode(" ", array_slice(explode(" ", $article->content), 0, 15));
        // //on passe ensuite le résultat dans nl2br et on y concatène une ellipse
        // $article_content = nl2br($truncated) . "…";

        $article_content = nl2br($article->content);
    
        echo "
            <article class='truncated'>
                <div class='main'>
                    <header>
                        <h2>{$article->title}</h2>
                        <h3>{$article->author}</h3>
                        <h4>{$article->created_at->format('Y-m-d H:i:s')}</h4>
                    </header>
                    <section>
                        <p>{$article_content}</p>
                    </section>
                    <footer>
                        {$footer}
                    </footer>
                </div>
                <a href='view.php?id={$article->id}'>Read More</a>
            </article>
        ";
    }
}