<?php

session_start();

if ($_SESSION['userid']) {
    $author = $_SESSION['userid'];
    
    if ($_GET['id']) {
        $id = $_GET['id'];

        include("db.php");
        $dbh = dbConnect();

        $stmt = $dbh->prepare("SELECT * FROM article WHERE id = :id AND author = :author");

        $stmt->execute(
            [
                ":id" => $id,
                ":author" => $author
            ]
        );

        if ($article = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $title = $article['title'];
            $content = $article['content'];
        } else {
            http_response_code(404);
            die('Article Not Found');
        }
    }
} else {
    header('Location: sign_in.php');
}

?>

<!DOCTYPE html>

<html>

    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>Update Article</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
    </head>

    <body>
        <form action="update_article_process.php" method="POST">
            <label for="title">
                Title
            </label>
            <input type="text" name="title" id="title" value="<?= $title ?>">
            <label for="content">
                Content
            </label>
            <textarea name="content" id="content"><?= $content ?></textarea>
            <input type="hidden" name="id" value="<?= $id ?>">
            <input type="submit" value="Update Article">
        </form>
    </body>

</html>