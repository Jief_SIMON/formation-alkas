<?php

session_start();

if ($_POST['userid']) {
    $author = $_SESSION['userid'];

    if (isset($_POST['title']) && isset($_POST['content'])) {
        $title = $_POST['title'];
        $content = $_POST['content'];

        if (!empty($title) && !empty($content)) {
            include('blogConnect.php');
            $dbh = dbConnect();

            $stmt = $dbh->prepare("INSERT INTO article (title, content, created_at, author) VALUES (:title, :content, :created_at, :author)");

            $date = date("Y-m-d H:i:s");

            $stmt->execute(
                [
                    ":title" => $title,
                    ":content" => $content,
                    ":created_at" => $date,
                    ":author" => $author
                ]
            );

            header('Location: index.php');     
        }
    }
} else {
    header('Location: sign_in.php');
}