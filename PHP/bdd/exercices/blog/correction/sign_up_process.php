<?php

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];

    if (!empty($username) && !empty($password) && !empty($password_confirm)) {
        if ($password === $password_confirm) {
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            include("blogConnect.php");
            $dbh = dbConnect();

            $stmt = $dbh->prepare("INSERT INTO user (username, password) VALUES (:username, :password)");
            $stmt->execute(
                [
                    ":username" => $username,
                    ":password" => $password_hash
                ]
            );

            $errors = $stmt->errorInfo();

            if ($errors[0] == "23000" && $errors[1] == 1062) {
                die("ERROR already existing username");
            }

            header('Location: sign_in.php');
        }
    }
}