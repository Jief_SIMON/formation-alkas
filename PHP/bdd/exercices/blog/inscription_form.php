<?php

session_start();

if (isset($_SESSION['userid'])) {
    header('Location: index.php');
}

$page_title = "Inscription";
include('head.php');

?>
<body>

<form action="inscription.php" method="POST">
    <fieldset>
        <legend>
            Inscription
        </legend>
        <label for="nickname">Pseudo : </label>
        <input type="text" name="nickname" id="nickname">
        <label for="password">Mot de passe : </label>
        <input type="text" name="password" id="password">
        <label for="password_repeat">Confirmation mot de passe : </label>
        <input type="text" name="password_repeat" id="password_repeat">
        <input type="submit" value="Inscription">
    </fieldset>
</form>

</body>
</html>