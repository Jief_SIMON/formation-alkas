<?php

require_once('blogConnect.php');

if (isset($_POST['nickname']) && isset($_POST['password']) && isset($_POST['password_repeat'])) {
    $_POST['nickname'] = $nickname;
    $_POST['password'] = $password;
    $_POST['password_repeat'] = $password_repeat;

    if (!empty($_POST['nickname']) && !empty($_POST['password']) && !empty($_POST['password_repeat'])) {

        if ($password === $password_hash) {
            $password_hash = password_hash($password, PASSWORD_DEFAULT);

            $dbh = dbConnect();

            $insert_user = "INSERT INTO ('nickname', 'password') VALUES (':nickname', ':password')";

            $stmt = $dbh->prepare($insert_user);

            $stmt->execute([
                ":nickname" => $nickname,
                ":password" => $password,
            ]);

            if ($stmt->errorInfo()[0] === "23000" && $stmt->errorInfo()[1] === 1062) {
                echo $nickname . "existe déjà !";
            }

            $_SESSION["userid"] = $insert_user["id"];

            header('Location: connexion_form.php');

        } else {
            echo "Pseudo ou mot de passe incorrect";
        }
    } else {
        echo "Erreur valeurs vides";
    }
} else {
    echo "Erreur dans le formulaire";
}