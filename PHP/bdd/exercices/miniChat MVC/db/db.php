<?php

include('model/user.php');
include('model/message.php');


/**
 * Raccorci pour instancier notre DatabaseHandler sur notre base de données
 */
function getDatabaseHandler()
{
    return new DatabaseHandler("chatboard", "minichat_user", "L9OImsgrEcvXRamI");
}

#region GESTION DE LA CONNEXION

/**
 * Permet l'utilisation de méthodes de dialogue avec la base de données
 */
class DatabaseHandler
{
    private string $_dbname;
    private string $_username;
    private string $_password;
    private PDO $_handler;

    public function __construct(string $dbname, string $username, string $password)
    {
        $this->_dbname = $dbname;
        $this->_username = $username;
        $this->_password = $password;

        $this->_handler = $this->dbConnect();
    }

    /**
     * Permet la connexion à la base de données en instanciant un objet PDO
     * 
     * L'objet instancié est rangé dans la propriété _handler du DatabaseHandler
     */
    public function dbConnect()
    {
        try {
            $dbh = new PDO("mysql:host=localhost;charset=utf8;dbname={$this->_dbname}", $this->_username, $this->_password);
        } catch (PDOException $e) {
            http_response_code(500);
            die("500 - Internal Server Error");
        }
        return $dbh;
    }

    /**
     * Renvoie l'instance de PDO utilisé par DatabaseHandler
     */
    public function getHandler()
    {
        return $this->_handler;
    }

    #endregion CONNEXION


    #region GESTION DE L'UTILISATEUR

    

    #endregion UTILISATEUR
}