<?php

Class ChatUser
{
    public $id;
    public $username;
    public $password;

    public function __construct(int $id, string $username, string $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    public function __toString(): string
    {
        return $this->username;
    }
}