<?php

Class ChatMessage
{
    public int $id;
    public string $content;
    public DateTime $sent_at;
    public ChatUser $author;

    public function __construct(int $id, string $content, DateTime $sent_at, ChatUser $author)
    {
        $this->$id = $id;
        $this->$content = $content;
        $this->$sent_at = $sent_at;
        $this->$author = $author->id;
    }
}