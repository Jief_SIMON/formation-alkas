<?php

session_start();
include('db/db.php');
include('display.php');

if ($_SESSION['userid']) {
        
?>

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>Message Board</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
            <script src='css/main.js'></script>
        </head>
        <body>
            <nav>
                <ul>
                    <li><a href="sign_out.php">Sign Out</a></li>
                </ul>
            </nav>
            <form action="post_message_process.php" method="POST">
                <fieldset>
                    <legend>
                        Post Message
                    </legend>
                    <label for="message">Message</label>
                    <textarea name="message" id="message" cols="20" rows="10"></textarea>
                    <input type="submit" value="Send Message">
                </fieldset>
            </form>

            <?php

            $dbh = getDatabaseHandler()->dbConnect();

            $messages = ("SELECT * FROM message ORDER BY sent_at DESC LIMIT 0, 20");
            
            $stmt = $dbh->prepare($messages);

            $stmt->execute();

            $res = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($res as $message) {
                echo sprintf("
                    <div class='message'>
                        <span><strong>
                            %s :
                        </strong></span>
                        <span>
                            %s
                        </span>
                        <span>
                            %s
                        </span>
                    </div>",
                    htmlspecialchars($message->author),
                    htmlspecialchars($message->content),
                    $message->sent_at
                );
            }

            ?>

        </body>
    </html>

<?php

} else {
    header('Location: Sign_in.php');
    exit;
}