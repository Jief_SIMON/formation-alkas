<?php

 session_start();

if (isset($_POST['message'])) {
    $content = $_POST['message'];
    $author = $_SESSION['userid'];

    if (!empty($content)) {
        include('db/db.php');

        $dbh = getDatabaseHandler()->dbConnect();

        $messages = ("INSERT INTO message (content, sent_at, author) VALUES (:content, :sent_at, :author)");

        $sent_at = date("Y-m-d H:i:s");
        $stmt = $dbh->prepare($messages);

        $stmt->execute(
            [
                ":content" => $content,
                ":sent_at" => $sent_at,
                ":author" => $author,
            ]
        );

        header('Location: index.php');
        exit;
    } else {
        echo "Post a valid message";
    }
} else {
    echo "Post a valid message";
}