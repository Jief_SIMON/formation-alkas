<?php

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];

    if (!empty($username) && !empty($password) && !empty($password_confirm)) {
        if ($password === $password_confirm) {
            $password_hash = password_hash($password, PASSWORD_DEFAULT);

            include('db/db.php');
            $dbh = getDatabaseHandler()->dbConnect();

            $stmt = $dbh->prepare("INSERT INTO user (username, password) VALUES (:username, :password)");

            $stmt->execute(
                [
                    ":username" => $username,
                    ":password" => $password_hash,
                ]
            );

            session_start();
            $_SESSION['userid'] = $user['id'];

            header('Location: index.php');
            exit;
        } else {
            echo "ERROR - Passwords don't match";
        }
    } else {
        echo "ERROR - Fields empty";
    }
} else {
    echo "ERROR - Form unvalid";
}