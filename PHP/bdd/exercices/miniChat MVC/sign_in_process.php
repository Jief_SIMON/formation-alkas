<?php

if (isset($_POST['username']) && ($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (!empty($username) && !empty($password)) {
        include('db/db.php');
        $dbh = getDatabaseHandler()->dbConnect();

        $getUser = "SELECT * FROM user WHERE username = :username";

        $stmt = $dbh->prepare($getUser);

        $stmt->execute(
            [
                ":username" => $username,
            ]
        );

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            if (password_verify($password, $user['password'])) {
                session_start();
                $_SESSION['userid'] = $user['id'];
                
                header('Location: index.php');
                exit;
            } else {
                echo "ERROR - Username or Password incorrect";
            }
        } else {
            echo "ERROR - Username or Password incorrect";
        }
    } else {
        echo "ERROR - Fields empty";
    }
} else {
    echo "ERROR - Form unvalid";
}