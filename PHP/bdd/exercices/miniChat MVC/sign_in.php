<?php

session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Sign In</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
    <script src='css/main.js'></script>
</head>
<body>
    <form action="sign_in_process.php" method="POST">
        <fieldset>
            <legend>
                Sign In
            </legend>
            <label for="username">Username</label>
            <input type="text" name="username" id="username">
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <input type="submit" value="Sign In">
        </fieldset>
    </form>
    <fieldset>
        <legend>
            Sign Up
        </legend>
        <a href="sign_up.php"><button>Sign Up</button></a>
    </fieldset>
</body>
</html>