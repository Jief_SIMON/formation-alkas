<?php

require_once('messageConnect.php');
require_once('index.php');

if (isset($_POST['message'])) {
    if (!empty($_POST['message'])) {
        $author = $_POST['nickname'];
        $message = $_POST['message'];

        $dbh = dbConnect("chatBoard", "messages");

        $new_message = "INSERT INTO message (content, sent_at, author) VALUES (:content, :sent_at, :author);";

        $stmt = $dbh->prepare($new_message);

        $sent_at = date('Y-m-d H:i:s');

        $stmt->execute([
            ":author" => $author,
            ":content" => $message,
            ":sent_at" => $sent_at,
        ]);

        header('Location: message.php');
    }
}