<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>MessageBoard</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>

<?php

require_once('index.php');
require_once('messageConnect.php');

?>

<body>
    <form action="send.php" method="POST">
        <fieldset>
            <legend>Informations Utilisateur</legend>
            <label for="nickname">
                Pseudo
            </label>
            <input id="nickname" name="nickname" type="text">
            <label>
                <?php
                    // $dbh = dbConnect("chatBoard", "messages");

                    // $user_name = "SELECT * FROM user WHERE :nickname";

                    // $stmt = $dbh->prepare($user_name);

                    // $stmt->execute([
                    //     ":nickname" => $nickname,
                    // ]);
                ?>
            </label>
        </fieldset>
        <fieldset>
            <legend>Envoyer un message</legend>
            <label for="message">
                Message
            </label>
            <input id="message" name="message" type="text">
        </fieldset>
        <input type="submit">

        <div class="messages">
            <?php
            foreach ($messages as $message) {
                echo sprintf(
                    "<span class='message'>
                    <span class='message-time'>
                        %s
                    </span>
                        %s
                    </span>
                    ",
                    $message->sent_at,
                    $message->content,
                );
            }
            ?>
        </div>
    </form>
</body>

</html>