<?php

require_once("messageConnect.php");

if (isset($_POST['nickname']) && isset($_POST['password']) && isset($_POST['password_repeat'])) {
    $nickname = $_POST['nickname'];
    $password = $_POST['password'];
    $password_repeat = $_POST['password_repeat'];

    if (!empty($nickname) && !empty($password) && !empty($password_repeat)) {
        if ($password === $password_repeat) {
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            
            $dbh = dbConnect("chatBoard", "messages");
            $insert_user = "INSERT INTO user (`nickname`, `password`) VALUES (:nickname, :password)";
            
            $stmt = $dbh->prepare($insert_user);
            
            $stmt->execute([
                ":nickname" => $nickname,
                ":password" => $password_hash
            ]);

            header('Location: message.php');
            
            if ($stmt->errorInfo()[0] === "23000" && $stmt->errorInfo()[1] === 1062) {
                echo $nickname . " existe déjà !";
            }
        } else {
            echo "erreur mots de passe";
        }
    } else {
        echo "erreur valeur vide";
    }
} else {
    echo "erreur formulaire";
}