<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>MessageBoard</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>

<?php

session_start();

require_once('messageConnect.php');

$dbh = dbConnect();

    $get_nickname = "SELECT * FROM user, message WHERE user.id = message.author";
    $stmt_get_nickname = $dbh->prepare($get_nickname);

    $stmt_get_nickname->execute();

    $nickname = $stmt_get_nickname->fetchAll(PDO::FETCH_OBJ);

    $get_messages = "SELECT * FROM message ORDER BY sent_at";
    $stmt_get_messages = $dbh->prepare($get_messages);

    $stmt_get_messages->execute();

    $messages = $stmt_get_messages->fetchAll(PDO::FETCH_OBJ);

    if (isset($_SESSION['user'])) {
        include('signin.php');
        ?>
            <body>
                <form action="signin.php" method="POST">
                    <label for="nickname">Pseudo</label>
                    <input id="nickname" name="nickname" type="text">
                    <label for="password">Mot de passe</label>
                    <input id="password" name="password" type="password">
                    <input type="submit" value="Connexion">
                </form>
            </body>
        <?php
    } else {
        include('signup.php');
        ?>
            <body>
                <form action="signup.php" method="POST">
                    <label for="nickname">Pseudo</label>
                    <input id="nickname" name="nickname" type="text">
                    <label for="password">Mot de passe</label>
                    <input id="password" name="password" type="password">
                    <label for="password_repeat">Confirmer mot de passe</label>
                    <input id="password_repeat" name="password_repeat" type="password">
                    <input type="submit" value="Inscription">
                </form>
                </body>
        <?php
    }