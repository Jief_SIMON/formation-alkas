# _"Message Board"_

Implémenter à l'aide d'une base de données un moyen d'envoyer des messages sur une page via un formulaire.
Les messages ainsi que l'expéditeur et l'heure d'expédition devront être stockés dans la base et lus depuis la base pour être affichés sur la page.