<?php

require_once("messageConnect.php");


if (isset($_SESSION['user'])) {
    if (isset($_POST['nickname']) && isset($_POST['password'])) {

        $nickname = $_POST['nickname'];
        $password = $_POST['password'];

        if (!empty($nickname) && !empty($password)) {

            $dbh = dbConnect("chatBoard", "messages");

            $get_user = "SELECT * FROM user WHERE nickname = :nickname";
            $stmt = $dbh->prepare($get_user);

            $stmt->execute([
                ":nickname" => $nickname
            ]);

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($user) { 
                if (password_verify($password, $user["password"])) {
                    echo "utilisateur connecté";
                    ?>
                    <a href="message.php"><input type="submit" value="Ecrire un message"></a>
                    <?php
                    header('Location: message.php');
                } else {
                    echo "nom d'utilisateur ou mot de passe erronés";
                }
            } else {
                echo "nom d'utilisateur ou mot de passe erronés";
            }
        } else {
            echo "erreur valeur vide";
        }
    } elseif (empty($_POST['nickname']) && empty($_POST['password'])) {
        ?>
        
        <?php
    } else {
        echo "erreur formulaire";
    } 
} else {
    header('Location: signup.php');
}