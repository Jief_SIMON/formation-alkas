<?php
//traitement du calcul

// Vérification de l'intégrité du formulaire.
if (isset($_POST['num1']) && isset($_POST['num2']) && isset($_POST['operation'])) {
    // Récupération de nos valeurs du formulaire.
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $operation = $_POST['operation'];

    // Si les opérandes sont bien des nombres.
    if (is_numeric($num1) && is_numeric($num2)) {
        // Application de l'opération.
        // Le switch-case permet de prendre une variable comme point de comparaison et établir plusieurs "cases".
        // Chaque "case" définit un comportement lorsque la variable à comparer est égale à une certaine valeur.
        // Les "break" permettent de mettre fin au switch si on rencontre un certain cas.
        switch($operation) {
            case 'add':
                echo $num1 + $num2;
                break;
            case 'sous':
                echo $num1 - $num2;
                break;
            case 'mul':
                echo $num1 * $num2;
                break;
            case 'div':
                if ($num2 !== 0) {
                    echo $num1 / $num2;
                } else {
                    echo "La division par 0 est impossible !";
                }
                break;
            default:
                echo "Opération {$operation} inconnue";
        }
    } else {
        echo "Les opérandes ne sont pas des nombres !";
    }
} else {
    echo "Erreur formulaire";
}