<?php
//calcul bande passante ici

if (isset($_POST['num']) && isset($_POST['unit1']) && isset($_POST['unit2'])) {
    // On les stocke
    $num = $_POST['num'];
    $unit1 = $_POST['unit1'];
    $unit2 = $_POST['unit2'];

    // On prépare des ratios de conversion en référence à notre unité de base : b/s
    // Etant donnée que le point de référence est le même pour tout les ratios dans notre tableau, le résultat est toujours calculé de façon relative et sera toujours correct.
    $ratios = ["b/s" => 1, "kb/s" => 1/1e3, "Mb/s" => 1/1e6, "Gb/s" => 1/1e9, "o/s" => 1/8, "ko/s" => 1/8e3, "Mo/s" => 1/8e6, "Go/s" => 1/8e9];

    // Pour vérifier que notre unité existe bien, on vérifie qu'elle existe dans les clés de notre tableau associatif.
    // A l'aide de array_key_exists()
    if (array_keys_exists($unit1, $ratios) && aray_key_exists($unit2, $ratios)) {
        echo $num * ($rations[$unit2]/$ratios[$unit1]);
    } else {
        echo "Unité inconnue";  
    }
}
