<?php
//traitement du formulaire ici 

// Vérification de l'intégrité de formulaire.
if (isset($_POST['nbValeurs']) && (isset($_POST['min']) && (isset($_POST['max'])) {
    $nbValeurs = $_POST['nbValeurs'];
    $min = $_POST['min'];
    $max = $_POST['max'];

    // On vérifie que les champs du formulaire aient bien été renseigné.
    // empty() vérifie qu'une variable soit vide ou pas, !empty() vérifie qu'une variable soit remplie.
    // Problème : empty considère 0 comme étant vide.
    if (!empty($nbValeurs) && !empty($min) && !empty($max)) {
        // Pour tester si un nombre est un nombre, on peut utiliser is_numeric().
        if (is_numeric($nbValeurs) && is_numeric($min && is_numeric($max)) {
            // Si notre nombre de valeurs demandé est positif.
            if ($nbValeurs >= 1) {
                // On répète l'opération autant de fois que nécessaire.
                if ($min > $max) {
                    echo "Votre valeur minimum est plus grande que votre valeur maximum";
                } else {
                    for ($i = 1; $i <= $_POST['nbValeurs']; $i++) {
                        $randomNum = rand($min, $max);
                        if ($i == 1) {
                            echo "<br/>Voici le {$i}er chiffre aléatoire : {$randomNum}";
                        } else {
                            echo "<br/>Voici le {$i}ème chiffre aléatoire : {$randomNum}";
                        }
                    }
                }
            } else {
                echo "Le nombre de valeurs à générer doit être supérieur à 0";
            }
        } else {
            echo "Les valeurs du formulaire doivent être des nombres";
        }
    } else {
        echo "Erreur formulaire";
    }
}