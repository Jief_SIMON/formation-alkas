<?php

// Utilisateur de la base de données
$user = "root";
// MdP de l'utilisateur
$pass = "";

// Variable qui permet de nous connecter à la base de données
$bdd = new PDO('mysql:host=localhost;dbname=cours_sql;charset=utf8', $user, $pass);

// Requête SQL
$sql = "SELECT * FROM villes_france_free LIMIT 50";
$reponse = $bdd->query($sql);

// Tant q'il y a des données, on affiche chaque ligne.
while ($data = $reponse->fetch()) {
    echo "Nom de la ville : ".$data['ville_nom']."<br>";
}

$reponse_2 = $bdd->query("SELECT * FROM villes_france_free WHERE ville_departement = 34 LIMIT 50");

while ($data = $reponse_2->fetch()) {
    echo "Nom d'une ville de l'Hérault : ".$data['ville_nom']."<br>";
}

// Requête paramétrable SQL

// Définition du paramètre

$recherche = "Mont%";
$departement = 34;

$sql_2 = "SELECT * 
        FROM villes_france_free 
        WHERE ville_nom LIKE ?
        AND ville_departement = ?
        ORDER BY ville_nom ASC 
        LIMIT 20";

$reponse_3 = $bdd->prepare($sql_2);
$reponse_3->execute(array($recherche, $departement));

while ($data = $reponse_3->fetch()) {
    echo "Ville commençant par Mont : ".$data['ville_nom']." Departement : ".$data['ville_departement']."<br>";
}

// Terminer la requête
$reponse = null;
$reponse_2 = null;
$reponse_3 = null;

// Terminer la connection
$bdd = null;