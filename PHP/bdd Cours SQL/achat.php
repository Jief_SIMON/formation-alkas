<?php

include('connexion.php');

$sql = "SELECT *
        FROM achats
        WHERE nom LIKE :cle_nom; 
        AND prenom LIKE :cle_prenom;";

$reponse = $bdd->prepare($sql);
$reponse->execute(array(
    'cle_nom' => 'Hester',
    'cle_prenom' => 'Maia'
));

while ($data = $reponse->fetch()) {
    echo "Client : ".$data['nom']." ".$data['prenom'].". Commande : ".$data['total_commande']."<br>";
}

// Terminer la requête
$reponse = null;
$reponse_2 = null;
$reponse_3 = null;

// Terminer la connection
$bdd = null;