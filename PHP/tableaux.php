<?php
// Historiquement, un tableau en php se crée à l'aide d'une fonction nommée array()
$nombres = array(1, 2, 3, 4); // dans $nombres sera stocké [1, 2, 3, 4];
// Depuis php5 on peut créer un tableau "à la volée" comme en JS.
$voitures = ["Fiat", "Ferrari", "Alfa Romeo"];
echo $voitures[1]; // Affiche Ferrari.

// Une grosse différence avec JS est que les tableaux php sont mutables (modifiables).
// Si on désire affecter une nouvelle valeur à une case du tableau, on peut agir comme n'importe quel
$voitures[0] = "Lancia";
echo "<br>";
echo $voitures[0];

// Pour ajouter une valeur à la suite d'un tableau, on peut utiliser des crochets vides.
$voitures[] = "Lamborghini";
echo "<br>";
echo $voitures[3];

// On peut utiliser également array_push(), qui permet d'ajouter plusieurs variables à la fois.

array_push($voitures, "Peugeot", "Renault");
// Pour les ajouter au début, on peut utiliser array_unshift();
array_unshift($voitures, "Mitsubishi", "Toyota");

// Si on désire afficher chaque élément de notre tableau, on peut utiliser un for comme d'hab.
// Pour obtenir le compte de valeurs dans notre tableau, on utilise count($tableau);

for ($i = 0; $i < count($voitures); $i++) {
    echo "<br>" . $voitures[$i];
}

// count sert aux tableaux et assimilés (collections) mais pas au string, les strings utilisent mb_strlen
// mb_strlen("coucou"); renvoie 6
// strlen() ne doit pas être utilisé (obsolète)

// En php, il existe aussi un autre type de tableaux appelé tableaux associatifs
// qui prennent la forme de dictionnaires clé/valeur.
$villes = [
    "34000" => "Montpellier",
    "75000" => "Paris",
    "31000" => "Toulouse",
    "13000" => "Marseille"
];

echo "<br>" . $villes["34000"]; // Renvoie "Montpellier"

// Les données de notre tableaux associatifs sont accessibles par des clés, mais plus par des indexs.
// Pour récupérer chaque élément d'un tableau associatif, on ne peut donc pas utiliser un for avec $i.
// On doit donc forcément utiliser un foreach($tableau as $cle => valeur){}
// Pour récupérer seulement les valeurs et pas les clés, on peut utiliser foreach($tableau as $valeur)
foreach ($villes as $code_postal => $villes) {
    echo "<br>" . $code_postal . " : " . $villes;
}

// Si on veut vérifier la présence d'une valeur dans un tableau, on utilise in_array()

if (in_array("Montpellier", $villes)) {
    echo "<br> Livraison possible à Montpellier";
};
// Pour vérifier une existence 