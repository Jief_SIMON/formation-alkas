<?php

// La fonction include permet d'inclure un morceau de code dans un autre.
// Ici, on permet l'éxécution de notre script personnage.php qui déclarera notre classe pour la rendre disponable dans notre scritp jeu.php.
include('personnage.php');
include('barbare.php'); // Barbare inclus déjà personnage, mais on utilise require_once
// On crée un personnage avec name, hp, et str.
$jacques = new Personnage('Jacques', 15, 3);
echo "Jacques a " . $jacques->getHp() . " points de vie";

$krom = new Barbare('Krom', 100, 0, 5);
echo 'La force du barbare est de ' . $krom->getStr();

echo Barbare::MIN_STR; // On peut accéder à une constante da classe en mettant NomDeLaClasse::CONSTANTE
// De cette façon on peut y accéder même sans instancier un objet à partir de cette classe.
echo Personnage::MIN_STR;