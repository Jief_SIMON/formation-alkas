<?php
//Exercice Compte En Banque
//Implémenter la gestion simple d'un compte en banque
//Un compte comprends les informations suivantes :
//Un nom de client et Un solde
//Un compte doit pouvoir effectuer les opérations suivantes : 
//Créditer un montant, 
//débiter un montant,
//afficher le nom du client et le solde du compte

class Compte {

    private $_name;
    private $_solde;
    private $_devise;

    public function __construct($name, $solde, $devise) {
        $this->_name = $name;
        $this->_solde = $solde;
        $this->_devise = $devise;
    }

    public function crediter($montant) {
        $this->_solde += $montant;
    }

    public function debiter($montant) {
        if ($this->_solde < $montant) {
            echo "Solde insuffisant";
        } else {
            $this->_solde -= $montant;
        }
    }

    // La méthode __toString() est une 'méthode magique' d'un objet en PHP.
    // Cette méthode sera appelée à chaque fois que l'objet devra être représenté en tant que string.
    // Cela permet par exemple de faire echo $notreObjet sans qu'une erreur apparaisse.
    public function __toString() {
        // Pour implémenter la méthode, il suffit de renvoyer une chaîne de caractères,
        // de préférence représentant l'objet en question.
        if ($this->_devise == "USD") {
            return "Le compte en banque de M./Mme. " . $this->_name . " a un solde de $ " . $this->_solde . ".";
        }
        if ($this->_devise == "EUR") {
            return "Le compte en banque de M./Mme. " . $this->_name . " a un solde de " . $this->_solde . " €.";
        }
    }
}

$compte = new Compte("Bernard", 200, "EUR");

// Etant donnée que __toString() est implémentée, on peut utiliser echo avec notre objet.
echo $compte;

$compte->crediter(100);
echo $compte;

$compte->debiter(2000);
echo $compte;