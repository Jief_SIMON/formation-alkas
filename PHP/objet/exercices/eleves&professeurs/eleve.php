<?php

require_once('personne.php');

class Eleve extends Personne {
    
    private $_niveau;

    public function __construct($prenom, $nom, $ddn, $niveau) {
        parent::__construct($prenom, $nom, $ddn);
        $this->_niveau = $niveau;
    }

    public function age() {
        // Pour gérer le calcul de l'âge, on utilise la fonction DateTime() de PHP.
        $maintenant = new DateTime(); // On crée la date du jour.
        $intervalle = $this->_ddn->diff($maintenant); // On crée la différence entre la date de naissance et la date du jour.
        return $intervalle->y; // Et on renvoie le nombre d'année de différence.
    }

    public function getNiveau() {
        return $this->_niveau;
    }

    public function __toString() {
        return parent::__toString() . " {$this->_niveau}";
    }
}