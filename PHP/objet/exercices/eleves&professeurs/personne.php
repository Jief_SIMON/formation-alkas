<?php

class Personne {

    protected string $_prenom;
    protected string $_nom;
    protected DateTime $_ddn;

    // Dans les paramètres d'une fonction, on peut préciser le type de données attendues en guise d'indication pour le développeur. Malheureusement cela n'oblige en rien à utiliser le bon type de donnée et PHP laissera passer même si on met le mauvais type.
    // ex: string $_prenom; DateTime $_ddn;
    public function __construct($prenom, $nom, $ddn){
        $this->_prenom = $prenom;
        $this->_nom = $nom;
        $this->_ddn = $ddn;
    }

    public function __toString() {
        return "{$this->_prenom} {$this->_nom} {$this->_ddn->format('Y-m-d')}";
    }
}