<?php

//Implémenter une gestion simple de cours particuliers
//Une classe Eleve avec nom prenom date de naissance et niveau d'etudes
//On doit pouvoir récupérer l'age de l'élève 
//Une classe Professeur avec nom prenom date de naissance et matiere enseignée
//On aura également une classe Cours qui contiendra un sujet, un professeur enseignant et un élève inscrit, et une date prévue 
//A partir du Cours, on doit pouvoir récupérer la description contenant Le professeur, sa matière, le sujet du cours, la date du cours, l'élève inscrit et son niveau d'études 

class Eleve {

    private $_name;
    private $_familyName;
    private $_dob;
    private $_studyLevel;

    public function __construct($name, $familyName, $dob, $studyLevel) {
        $this->setName($name);
        $this->setFamilyName($familyName);
        $this->setDob($dob);
        $this->setStudyLevel($studyLevel);
    }

    public function age($age) {
        $age = date('Y') - $this->_dob;
        return $age;
    }

    public function getName() {
        return $this->_name;
    }

    public function getFamilyName() {
        return $this->_familyName;
    }

    public function getDob() {
        return $this->_dob;
    }

    public function getStudyLevel() {
        return $this->_studyLevel;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setFamilyName($familyName) {
        $this->_familyName = $familyName;
    }

    public function setDob($dob) {
        $this->_dob = $dob;
    }

    public function setStudyLevel($studyLevel) {
        $this->_studyLevel = $studyLevel;
    }

}

$year = date('Y');

$nouvelEleve = new Eleve("Eric", "MARTIN", 1986, "Bac + 2");
echo "L'élève " . $nouvelEleve->getName() . " " . $nouvelEleve->getFamilyName() . " a " . (($year) - ($nouvelEleve->getDob())) . " ans.";

class Professeur {

    private $_name;
    private $_familyName;
    private $_dob;
    private $_schoolSubject;

    public function __construct($name, $familyName, $dob, $schoolSubject) {
        $this->setName($name);
        $this->setFamilyName($familyName);
        $this->setDob($dob);
        $this->setSchoolSubject($schoolSubject);
    }

    public function getName() {
        return $this->_name;
    }

    public function getFamilyName() {
        return $this->_familyName;
    }

    public function getDob() {
        return $this->_dob;
    }

    public function getSchoolSubject() {
        return $this->_schoolSubject;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setFamilyName($familyName) {
        $this->_familyName = $familyName;
    }

    public function setDob($dob) {
        $this->_dob = $dob;
    }

    public function setSchoolSubject($schoolSubject) {
        $this->_schoolSubject = $schoolSubject;
    }
}

class Cours {

    private $_subject;
    private $_coursDay;

    
}