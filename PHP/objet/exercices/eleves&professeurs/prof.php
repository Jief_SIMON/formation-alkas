<?php

require_once('personne.php');

class Professeur extends Personne {

    private $_matiere;

    public function __construct($prenom, $nom, $ddn, $matiere) {
        parent::__construct($prenom, $nom, $ddn);
        $this->_matiere = $matiere;
    }
    
    public function getMatiere() {
        return $this->_matiere;
    }

    public function __toString() {
        return parent::__toString() . " {$this->_matiere}";
    }
}