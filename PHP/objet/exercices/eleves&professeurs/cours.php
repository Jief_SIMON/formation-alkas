<?php

require_once('eleve.php');
require_once('prof.php');

class Cours {

    private $_sujet;
    private $_date;
    private $_prof;
    private $_eleve;

    public function __construct($sujet, $date, $prof, $eleve) {
        $this->_sujet = $sujet;
        $this->_date = $date;
        $this->_prof = $prof;
        $this->_eleve = $eleve;
    }

    public function __toString() {
        return "{$this->_sujet} {$this->_date->format('Y-m-d')} {$this->_prof} {$this->_eleve}";
    }
}

$prof = new Professeur('John', 'Doe', new DateTime('1995-11-11'), 'Chimie');
$eleve = new Eleve('Jane', 'Doe', new DateTime('2000-05-14'), 'Bac');
$cours = new Cours('Chimime Organique', new DateTime('2018-10-20'), $prof, $eleve);

echo $cours;