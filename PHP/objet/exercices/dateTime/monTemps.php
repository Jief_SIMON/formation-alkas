<?php

class MonTemps
{
    private int $_heure;
    private int $_minute;
    private int $_seconde;

    public function __construct(int $heure, int $minute, int $seconde)
    {
        $this->setHeure($heure);
        $this->setMinute($minute);
        $this->setSeconde($seconde);
    }

    // Getters

    public function getHeure() : int
    {
        return $this->_heure;
    }
    
    public function getMinute() : int
    {
        return $this->_minute;
    }
    
    public function getSeconde() : int
    {
        return $this->_seconde;
    }

    // Setters

    public function setHeure(int $heure) : void
    {
        if ($heure >= 0 && $heure <= 23) {
            $this->_heure = $heure;
        } else {
            echo "L'heure est invalide et doit être comprise entre 0 et 24";
        }
    }

    public function setMinute(int $minute) : void
    {
        if ($minute >= 0 && $minute <= 59) {
            $this->_minute = $minute;
        } else {
            echo "Le nombre de minute est invalide et doit être compris entre 0 et 59";
        }
    }

    public function setSeconde(int $seconde) : void
    {
        if ($seconde >= 0 && $seconde <= 59) {
            $this->_seconde = $seconde;
        } else {
            echo "Le nombre de seconde est invalide et doit être compris entre 0 et 59";
        }
    }

    // Incrémenteurs

    public function heureSuivante()
    {
        if ($this->getHeure() === 23) {
            $this->setHeure(0);
        } else {
            $this->setHeure($this->getHeure() + 1);
        }
    }

    public function minuteSuivante()
    {
        if ($this->getMinute() === 59) {
            $this->heureSuivante();
            $this->setMinute(0);
        } else {
            $this->setMinute($this->getMinute() + 1);
        }
    }

    public function secondeSuivante()
    {
        if ($this->getSeconde() === 59) {
            $this->minuteSuivante();
            $this->setSeconde(0);
        } else {
            $this->setSeconde($this->getSeconde() + 1);
        }
    }

    // Décrémenteurs

    public function heurePrecedente()
    {
        if ($this->getHeure() === 0) {
            $this->setHeure(23);
        } else {
            $this->setHeure($this->getHeure() - 1);
        }
    }

    public function minutePrecedente()
    {
        if ($this->getMinute() === 0) {
            $this->heurePrecedente();
            $this->setMinute(59);
        } else {
            $this->setMinute($this->getMinute() - 1);
        }
    }

    public function secondePrecedente()
    {
        if ($this->getSeconde() === 0) {
            $this->minutePrecedente();
            $this->setSeconde(59);
        } else {
            $this->setSeconde($this->getSeconde() - 1);
        }
    }

    public function afficher()
    {
        return sprintf('%02d:%02d:%02d', $this->getHeure(), $this->getMinute(), $this->getSeconde());
    }

    public function __toString() : string
    {
        return sprintf('%02d:%02d:%02d', $this->getHeure(), $this->getMinute(), $this->getSeconde());
    }
}

$maintenant = new MonTemps(17, 23, 0);
$maintenant->heurePrecedente();
$maintenant->minutePrecedente();
$maintenant->secondePrecedente();
echo $maintenant;
$maintenant->afficher();