<?php

class MaDate 
{
    private int $_jour;
    private int $_mois;
    private int $_annee;
    
    public function __construct(int $annee, int $mois, int $jour) 
    {
        $this->reglerAnnee($annee);
        $this->reglerMois($mois);
        $this->reglerJour($jour);
    }

    // Getters
    public function recupAnnee() {
        return $this->_annee;
    }
    
    public function recupMois() {
        return $this->_mois;
    }
    
    public function recupJour() {
        return $this->_jour;
    }
    
    // Setters
    public function reglerAnnee($annee)
    {
        if ($annee >= 1 && $annee <= 9999) {
            $this->_annee = $annee;
        } else {
            echo "Erreur année invalide : doit être comprise entre 1 et 9999.";
        }
    }
    
    public function reglerMois($mois) 
    {
        if ($mois >= 1 && $mois <= 12) {
            $this->_mois = $mois;
            if (isset($this->_jour) && $this->recupJour() > $this->recupJourMax()) {
                echo "Erreur jour invalide : le jour doit être inférieur à : " . $this->recupJourMax();
            }
        } else {
            echo "Le mois est invalide : doit être compris entre 1 et 12.";
        }
    }

    public function reglerJour($jour) 
    {
        if ($jour >= 1 && $jour <= $this->recupJourMax()) {
            $this->_jour = $jour;
        } else {
            echo "Le jour est invalide, doit être compris entre 1 et " . $this->recupJourMax();
        }
    }
    
    // Incrémenteurs

    public function anneeSuivante()
    {
        $this->reglerAnnee($this->recupAnnee() + 1);
    }

    public function moisSuivant()
    {
        if ($this->recupMois() === 12) {
            $this->anneeSuivante();
            $this->reglerMois(1);
        } else {
            $this->reglerMois($this->recupMois() + 1);
        }
    }

    public function jourSuivant()
    {
        if ($this->recupJour() === $this->recupJourMax()){
            $this->reglerJour(1);
            $this->moisSuivant();
        } else {
            $this->reglerJour($this->recupJour() + 1);
        }
    }

    // Décrémenteurs


    
    private const JOURS_MOIS = [1 => 31, 2 => 28, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];


    public function recupJourMax() : int
    {
        if ($this->anneeEstBissextile() && $this->recupMois() === 2) {
            return 29;
        } else {
            return self::JOURS_MOIS[$this->recupMois()];
        }
    }

    private function anneeEstBissextile() : bool
    {
        return ($this->recupAnnee() % 4 === 0 && $this->recupAnnee() % 100 !== 0 || $this->recupAnnee() % 400 === 0);
    }

    public function afficher() : string
    {
        return sprintf('%d-%02d-%02d', $this->recupJour(), $this->recupMois(), $this->recupAnnee());
    }

    private const JOURS = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    public function jourSemaine() : string
    {
        return self::JOURS[0];
    }
    

    public function joursAvantAnnee()
    {
        $annee = $this->recupAnnee() - 1;
        return $annee * 365 + $annee % 4 - $annee % 100 + $annee % 400;
    }
}

//try {
$dateDuJour = new MaDate(1984, 12, 12);
$dateDuJour->afficher();
//} catch (Exception $e) {
//    echo "Oups";
//} finally {

//}