<?php

// reequire_once est un include obligatoire mais qui ne doit être importé que s'il n'a pas déjà été importé auparavant.
require_once("personnage.php");
// L'héritage se fait à l'aide du mot clé extends.
class Barbare extends Personnage {
    // On peut rajouter de nouvelles propriétés par rapport à Personnage.
    private $_rage = 100;
    private $_resistance;

    // On peut sucharger (override) une constante déjà existante en la redéfinissant.
    const MIN_STR = 3;

    // Si on a besoin de préciser de nouveaux attributs dans un constructeur, ou définir de nouvelles valeurs à l'instantiation.
    // On peut décider d'appeler __construct
    public function __construct($name, $hp, $str, $resistance) {
        // Pour construire un Barbare, il faut construire un personnage.
        // On doit donc appeler le constructeur de Personnage, aui est de classe parent.
        // parent:: est l'équivalent de super en JS.
        parent::__construct($name, $hp, $str);
        // Maintenant que le constructeur parent a été appelé, on peut attribuer les valeurs à notre Barbare.
        $this->_resistance = $resistance;
    }

    // On peut ensuite déterminer les getters/setters de notre classe héritante sans avoir besoin de redéfinir ce qui appartient à Personnage.
    public function getRage() {
        return $this->_rage;
    }

    public function getResistance() {
        return $this->_resistance;
    }

    public function setRage() {
        $this->_rage = $rage;
    }

    public function setResistance() {
        $this->_resistance = $resistance;
    }
}