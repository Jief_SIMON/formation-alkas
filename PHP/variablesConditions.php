<?php

// Les variables ont leur déclaration (et nom) précédées de $
$message = "Salut";

// Etant donnée qu'un fichier php interprété renvoie toujours du texte.
// Pour afficher quelque chose à l'écran, il faut pouvoir écrire du texte dans le document.
// Pour cela, on utilise la fonction echo :
echo $message;

$nombre = 15;

echo nl2br("\nLe nombre est $nombre + 5"); // Pour sauter une ligne, on peut associer le caractère \n (new line) avec la fonction nl2br, qui traduit les \n en <br>.
// Pour insérer une variable dans une chaîne de caractères définie avec des doubles quotes(") est possible,
// mais parfois pour améliorer la lisibilité ouveffectuer certaines opérations, on va utiliser la concaténation.

// La concaténation en php se fait avec .
echo nl2br("\nLe nombre est " . ($nombre + 5));

$age = 11;

echo "<p>"; // Pour écrire 
if ($age < 18) {
    echo "Accès interdit";
} elseif ($age > 20) {
    echo "Vous pouvez boire";
} else {
    echo "On regarde mais on ne boit pas";
}
echo "</p>";